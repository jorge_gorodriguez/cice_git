﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSize : MonoBehaviour {

    Vector3 scaleStep;
    int resourceAmount;
	// Use this for initialization
	void Start () {
        resourceAmount = 10;
        scaleStep = transform.localScale / resourceAmount;
        StartCoroutine(DegrowStep(scaleStep, 1f));
        //for (int i = 0; i < resourceAmount; i++) {
        //    StartCoroutine(DegrowStep(scaleStep, 1f));
        //    if (transform.localScale.x <= 0 || transform.localScale.y <= 0 || transform.localScale.z <= 0) {
        //        Destroy(this.gameObject);
        //    }
        //}
    }
	
	// Update is called once per frame
	void Update () {
        //transform.localScale += new Vector3(0.1f, 0.05f, 0.1f) * Time.deltaTime;
    }

    IEnumerator Degrow(float duration) {
        float elapsedTime = 0;
        GameObject auxParent = new GameObject();
        auxParent.transform.parent = transform.parent;
        transform.parent = auxParent.transform;
        while (elapsedTime < duration) {
            Debug.Log("growing");
            elapsedTime += Time.deltaTime;            
            transform.localScale -= new Vector3(0.05f, 0.025f, 0.05f) * Time.deltaTime;
            auxParent.transform.localPosition -= new Vector3(0f, 0.025f / 2, 0f) * Time.deltaTime;
            yield return null;            
        }
        transform.parent = auxParent.transform.parent;
        Destroy(auxParent);
    }

    IEnumerator DegrowStep(Vector3 scaleStep, float duration) {
        GameObject auxParent = new GameObject();
        auxParent.transform.parent = transform.parent;
        auxParent.transform.position = transform.position;
        auxParent.transform.localScale = Vector3.one;
        transform.parent = auxParent.transform;

        Vector3 startingLocalScale = transform.localScale;
        Vector3 targetLocalScale = startingLocalScale - scaleStep;

        Vector3 positionStep = new Vector3(0f, scaleStep.y / 2, 0f);
        Vector3 startingLocalPosition = auxParent.transform.localPosition;
        Vector3 targetLocalPosition = startingLocalPosition - positionStep;
        float elapsedTime = 0;
        while (elapsedTime < duration) {
            elapsedTime += Time.deltaTime;
            float blend = Mathf.Clamp01(elapsedTime / duration);

            Vector3 localScale = transform.localScale;
            localScale.x = Mathf.Lerp(startingLocalScale.x, targetLocalScale.x, blend);
            localScale.y = Mathf.Lerp(startingLocalScale.y, targetLocalScale.y, blend);
            localScale.z = Mathf.Lerp(startingLocalScale.z, targetLocalScale.z, blend);
            transform.localScale = localScale;

            Vector3 localPosition = auxParent.transform.localPosition;
            localPosition.x = Mathf.Lerp(startingLocalPosition.x, targetLocalPosition.x, blend);
            localPosition.y = Mathf.Lerp(startingLocalPosition.y, targetLocalPosition.y, blend);
            localPosition.z = Mathf.Lerp(startingLocalPosition.z, targetLocalPosition.z, blend);
            auxParent.transform.localPosition = localPosition;
            yield return null;
        }
        transform.parent = auxParent.transform.parent;
        Destroy(auxParent);
    }
}
