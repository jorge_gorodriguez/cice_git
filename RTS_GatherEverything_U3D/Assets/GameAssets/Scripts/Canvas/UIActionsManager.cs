﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum TooltipType {
    BUILDING_COST,
    TOWER_COST,
    UNIT_COST,
    NAME
}

public class UIActionsManager : MonoBehaviour {
    #region VARIABLES
    [Header("Section Groups")]
    public GameObject buttonsGroup;
    public GameObject menusGroup;

    [Header("Menu Buttons")]
    public Button buildingsMenuButton;
    public Button towersMenuButton;
    public Button unitsMenuButton;
    public Button workersMenuButton;
    public Button backButton;
    public Button exitButton;

    [Header("Speed Buttons")]
    public Button x1SpeedButton;
    public Button x2SpeedButton;

    [Header("Round Info")]
    public Text roundNameText;
    public Transform roundInfoPanelHolder;
    public GameObject roundInfoPanelPrefab;    

    [Header("Next Round Button")]
    public Button nextRoundButton;

    [Header("Building Buttons")]
    public Button buildWoodHouseButton;
    public Button buildStoneHouseButton;
    public Button buildBigHouseButton;
    public Button buildMineButton;
    public Button buildSawmillButton;
    public Button buildFarmButton;
    public Button buildWorkshopButton;
    public Button buildWaterwellButton;

    [Header("Tower Buttons")]
    public Button buildBaseTowerButton;
    public Button buildSporeTowerButton;
    public Button buildSpikeTowerButton;
    public Button buildVolcanoTowerButton;
    public Button buildSnowTowerButton;

    [Header("Units Buttons")]
    public Button createWorkerButton;
    public Button createWarriorButton;
    public Button createDefenderButton;
    public Button createGymOneButton;

    [Header("Workers Buttons and Texts")]
    public Text unemployedAmountText;
    public Button addBuilderButton;
    public Button subBuilderButton;
    public Text buildersAmountText;
    public Button addMinerButton;
    public Button subMinerButton;
    public Text minersAmountText;
    public Button addWoodcutterButton;
    public Button subWoodcutterButton;
    public Text woodcuttersAmountText;
    public Button addFarmerButton;
    public Button subFarmerButton;
    public Text farmersAmountText;
    public Button addWaterMastersButton;
    public Button subWaterMastersButton;
    public Text waterMastersAmountText;

    [Header("Panels")]
    public GameObject buildingsPanel;
    public GameObject towersPanel; 
    public GameObject unitsPanel;
    public GameObject workersPanel;

    [Header("Building Cost Tooltips")]
    public GameObject costTooltip;
    public Text goldBuildingCostTooltipText;
    public Text woodBuildingCostTooltipText;
    public Text foodBuildingCostTooltipText;
    public Text waterBuildingCostTooltipText;
    public Text populationBuildingCostTooltipText;
    public Text populationBuildingBenefitTooltipText;
    public Text buildersBuildingBenefitTooltipText;
    public Text minersBuildingBenefitTooltipText;
    public Text woodcuttersBuildingBenefitTooltipText;
    public Text farmersBuildingBenefitTooltipText;
    public Text waterMastersBuildingBenefitTooltipText;
    public Text contentBuildingCostTooltipText;

    [Header("Tower Cost Tooltips")]
    public GameObject towerCostTooltip;
    public Text goldTowerCostTooltipText;
    public Text woodTowerCostTooltipText;
    public Text foodTowerCostTooltipText;
    public Text waterTowerCostTooltipText;
    public Text populationTowerCostTooltipText;
    public Text damageTowerBenefitTooltipText;
    public Text rangeTowerBenefitTooltipText;
    public Text fireRateTowerBenefitTooltipText;    
    public Text contentTowerCostTooltipText;

    [Header("Units Cost Tooltips")]
    public GameObject unitCostTooltip;
    public Text goldUnitCostTooltipText;
    public Text woodUnitCostTooltipText;
    public Text foodUnitCostTooltipText;
    public Text waterUnitCostTooltipText;
    public Text populationUnitCostTooltipText;
    public Text damageUnitBenefitTooltipText;
    public Text healthUnitBenefitTooltipText;
    public Text speedUnitBenefitTooltipText;
    public Text contentUnitCostTooltipText;

    [Header("Informative Tooltips")]
    public GameObject contentTooltip;
    public Text contentToolTipText;
    #endregion
    public void AssignMenuButtonsListeners() {
        //Menu Buttons
        buildingsMenuButton.onClick.AddListener(delegate { ShowBuildingsMenu(true); });
        towersMenuButton.onClick.AddListener(delegate { ShowTowersMenu(true); });
        unitsMenuButton.onClick.AddListener(delegate { ShowUnitsMenu(true); });
        workersMenuButton.onClick.AddListener(delegate { ShowWorkersMenu(true); });
        exitButton.onClick.AddListener(delegate { GameManager.Instance.EndGame(); });
        //Building Buttons
        buildWoodHouseButton.onClick.AddListener(delegate { PlaceBuildingListener(BuildingTypes.WOOD_HOUSE); });
        buildStoneHouseButton.onClick.AddListener(delegate { PlaceBuildingListener(BuildingTypes.STONE_HOUSE); }); ;
        buildBigHouseButton.onClick.AddListener(delegate { PlaceBuildingListener(BuildingTypes.BIG_HOUSE); }); ;
        buildSawmillButton.onClick.AddListener(delegate { PlaceBuildingListener(BuildingTypes.SAWMILL); });
        buildMineButton.onClick.AddListener(delegate { PlaceBuildingListener(BuildingTypes.MINE); });
        buildFarmButton.onClick.AddListener(delegate { PlaceBuildingListener(BuildingTypes.FARMHOUSE); });
        buildWorkshopButton.onClick.AddListener(delegate { PlaceBuildingListener(BuildingTypes.WORKSHOP); });
        buildWaterwellButton.onClick.AddListener(delegate { PlaceBuildingListener(BuildingTypes.WATERWELL); });
        //TowerButtons
        buildBaseTowerButton.onClick.AddListener(delegate { PlaceTowerListener(TowerTypes.BASE_TOWER); });
        buildSporeTowerButton.onClick.AddListener(delegate { PlaceTowerListener(TowerTypes.SPORE_TOWER); } );
        buildSpikeTowerButton.onClick.AddListener(delegate { PlaceTowerListener(TowerTypes.SPIKE_TOWER); } );
        buildVolcanoTowerButton.onClick.AddListener(delegate { PlaceTowerListener(TowerTypes.VOLCANO_TOWER); } );
        buildSnowTowerButton.onClick.AddListener(delegate { PlaceTowerListener(TowerTypes.SNOW_TOWER); } );
        //UnitsButtons
        createWorkerButton.onClick.AddListener(delegate { PlaceUnitListener(UnitTypes.WORKER); });
        createWarriorButton.onClick.AddListener(delegate { PlaceUnitListener(UnitTypes.WARRIOR); });
        createDefenderButton.onClick.AddListener(delegate { PlaceUnitListener(UnitTypes.DEFENDER); });
        createGymOneButton.onClick.AddListener(delegate { PlaceUnitListener(UnitTypes.GYM_ONE); });        
        //WorkersButtons
        addBuilderButton.onClick.AddListener(delegate { AddWorkerToJobListener(WorkerJob.BUILDER); });
        subBuilderButton.onClick.AddListener(delegate { SubWorkerToJobListener(WorkerJob.BUILDER); });
        addMinerButton.onClick.AddListener(delegate { AddWorkerToJobListener(WorkerJob.MINER); });
        subMinerButton.onClick.AddListener(delegate { SubWorkerToJobListener(WorkerJob.MINER); });
        addWoodcutterButton.onClick.AddListener(delegate { AddWorkerToJobListener(WorkerJob.WOODCUTTER); });
        subWoodcutterButton.onClick.AddListener(delegate { SubWorkerToJobListener(WorkerJob.WOODCUTTER); });
        addFarmerButton.onClick.AddListener(delegate { AddWorkerToJobListener(WorkerJob.FARMER); });
        subFarmerButton.onClick.AddListener(delegate { SubWorkerToJobListener(WorkerJob.FARMER); });
        addWaterMastersButton.onClick.AddListener(delegate { AddWorkerToJobListener(WorkerJob.WATER_MASTER); });
        subWaterMastersButton.onClick.AddListener(delegate { SubWorkerToJobListener(WorkerJob.WATER_MASTER); });
        //Speed Buttons
        x1SpeedButton.onClick.AddListener(delegate { GameManager.Instance.SetGameSpeed(1); });
        x2SpeedButton.onClick.AddListener(delegate { GameManager.Instance.SetGameSpeed(2); });
        //StartRound Button
        nextRoundButton.onClick.AddListener(delegate { StartRoundListener(); });
        //Tooltip Texts
        //For Menus
        SetTriggerEventTo(buildingsMenuButton.gameObject, TooltipType.NAME, Tooltips.BUILDING_MENU);
        SetTriggerEventTo(towersMenuButton.gameObject, TooltipType.NAME, Tooltips.TOWERS_MENU);
        SetTriggerEventTo(unitsMenuButton.gameObject, TooltipType.NAME, Tooltips.UNITS_MENU);
        SetTriggerEventTo(workersMenuButton.gameObject, TooltipType.NAME, Tooltips.WORKERS_MENU);
        SetTriggerEventTo(backButton.gameObject, TooltipType.NAME, Tooltips.BACK);
        SetTriggerEventTo(nextRoundButton.gameObject, TooltipType.NAME, Tooltips.NEXT_ROUND);
        //For Buildings
        SetTriggerEventTo(buildWoodHouseButton.gameObject, TooltipType.BUILDING_COST, Tooltips.BUILD_WOOD_HOUSE, GameManager.Instance.buildingManager.buildingsCostAtlas[BuildingTypes.WOOD_HOUSE], GameManager.Instance.buildingManager.woodHousePrefab.GetComponent<House>());
        SetTriggerEventTo(buildStoneHouseButton.gameObject, TooltipType.BUILDING_COST, Tooltips.BUILD_STONE_HOUSE, GameManager.Instance.buildingManager.buildingsCostAtlas[BuildingTypes.STONE_HOUSE], GameManager.Instance.buildingManager.stoneHousePrefab.GetComponent<House>());
        SetTriggerEventTo(buildBigHouseButton.gameObject, TooltipType.BUILDING_COST, Tooltips.BUILD_BIG_HOUSE, GameManager.Instance.buildingManager.buildingsCostAtlas[BuildingTypes.BIG_HOUSE], GameManager.Instance.buildingManager.bigHousePrefab.GetComponent<House>());
        SetTriggerEventTo(buildMineButton.gameObject, TooltipType.BUILDING_COST, Tooltips.BUILD_MINE, GameManager.Instance.buildingManager.buildingsCostAtlas[BuildingTypes.MINE], GameManager.Instance.buildingManager.minePrefab.GetComponent<Mine>());
        SetTriggerEventTo(buildSawmillButton.gameObject, TooltipType.BUILDING_COST, Tooltips.BUILD_SAWMILL, GameManager.Instance.buildingManager.buildingsCostAtlas[BuildingTypes.SAWMILL], GameManager.Instance.buildingManager.sawmillPrefab.GetComponent<Sawmill>());
        SetTriggerEventTo(buildFarmButton.gameObject, TooltipType.BUILDING_COST, Tooltips.BUILD_FARM, GameManager.Instance.buildingManager.buildingsCostAtlas[BuildingTypes.FARMHOUSE], GameManager.Instance.buildingManager.farmPrefab.GetComponent<Farmhouse>());
        SetTriggerEventTo(buildWorkshopButton.gameObject, TooltipType.BUILDING_COST, Tooltips.BUILD_WORKSHOP, GameManager.Instance.buildingManager.buildingsCostAtlas[BuildingTypes.WORKSHOP], GameManager.Instance.buildingManager.workshopPrefab.GetComponent<Workshop>());
        SetTriggerEventTo(buildWaterwellButton.gameObject, TooltipType.BUILDING_COST, Tooltips.BUILD_WATERWELL, GameManager.Instance.buildingManager.buildingsCostAtlas[BuildingTypes.WATERWELL], GameManager.Instance.buildingManager.waterwellPrefab.GetComponent<Waterwell>());
        //For Towers
        SetTriggerEventTo(buildBaseTowerButton.gameObject, TooltipType.TOWER_COST, Tooltips.BUILD_BASE_TOWER, GameManager.Instance.buildingManager.towersCostAtlas[TowerTypes.BASE_TOWER], GameManager.Instance.buildingManager.baseTowerPrefab.GetComponent<BaseTower>());
        SetTriggerEventTo(buildSporeTowerButton.gameObject, TooltipType.TOWER_COST, Tooltips.BUILD_SPORE_TOWER, GameManager.Instance.buildingManager.towersCostAtlas[TowerTypes.SPORE_TOWER], GameManager.Instance.buildingManager.sporeTowerPrefab.GetComponent<SporeTower>());
        SetTriggerEventTo(buildSpikeTowerButton.gameObject, TooltipType.TOWER_COST, Tooltips.BUILD_SPIKE_TOWER, GameManager.Instance.buildingManager.towersCostAtlas[TowerTypes.SPIKE_TOWER], GameManager.Instance.buildingManager.spikeTowerPrefab.GetComponent<SpikeTower>());
        SetTriggerEventTo(buildVolcanoTowerButton.gameObject, TooltipType.TOWER_COST, Tooltips.BUILD_VOLCANO_TOWER, GameManager.Instance.buildingManager.towersCostAtlas[TowerTypes.VOLCANO_TOWER], GameManager.Instance.buildingManager.volcanoTowerPrefab.GetComponent<VolcanoTower>());
        SetTriggerEventTo(buildSnowTowerButton.gameObject, TooltipType.TOWER_COST, Tooltips.BUILD_SNOW_TOWER, GameManager.Instance.buildingManager.towersCostAtlas[TowerTypes.SNOW_TOWER], GameManager.Instance.buildingManager.snowTowerPrefab.GetComponent<SnowTower>());
        //For Units
        SetTriggerEventTo(createWorkerButton.gameObject, TooltipType.UNIT_COST, Tooltips.CREATE_WORKER, GameManager.Instance.unitManager.unitsCostAtlas[UnitTypes.WORKER], GameManager.Instance.unitManager.workerPrefab.GetComponent<Unit>());
        SetTriggerEventTo(createWarriorButton.gameObject, TooltipType.UNIT_COST, Tooltips.CREATE_WARRIOR, GameManager.Instance.unitManager.unitsCostAtlas[UnitTypes.WARRIOR], GameManager.Instance.unitManager.warriorPrefab.GetComponent<Unit>());
        SetTriggerEventTo(createDefenderButton.gameObject, TooltipType.UNIT_COST, Tooltips.CREATE_DEFENDER, GameManager.Instance.unitManager.unitsCostAtlas[UnitTypes.DEFENDER],GameManager.Instance.unitManager.defenderPrefab.GetComponent<Unit>() );
        SetTriggerEventTo(createGymOneButton.gameObject, TooltipType.UNIT_COST, Tooltips.CREATE_GYM_ONE, GameManager.Instance.unitManager.unitsCostAtlas[UnitTypes.GYM_ONE], GameManager.Instance.unitManager.gymOnePrefab.GetComponent<Unit>());

    }

    //Esconder y ocultar menus
    public void ShowBuildingsMenu(bool isActive) {
        buttonsGroup.SetActive(!isActive);
        menusGroup.SetActive(isActive);
        buildingsPanel.SetActive(isActive);
        backButton.onClick.RemoveAllListeners();
        if (isActive) { backButton.onClick.AddListener(delegate { ShowBuildingsMenu(false); }); }
    }
    public void ShowTowersMenu(bool isActive) {
        buttonsGroup.SetActive(!isActive);
        menusGroup.SetActive(isActive);
        towersPanel.SetActive(isActive);
        backButton.onClick.RemoveAllListeners();
        if (isActive) { backButton.onClick.AddListener(delegate { ShowTowersMenu(false); }); }
    }
    public void ShowUnitsMenu(bool isActive) {
        buttonsGroup.SetActive(!isActive);
        menusGroup.SetActive(isActive);
        unitsPanel.SetActive(isActive);
        backButton.onClick.RemoveAllListeners();
        if (isActive) { backButton.onClick.AddListener(delegate { ShowUnitsMenu(false); }); }
    }
    public void ShowWorkersMenu(bool isActive) {
        buttonsGroup.SetActive(!isActive);
        menusGroup.SetActive(isActive);
        workersPanel.SetActive(isActive);
        backButton.onClick.RemoveAllListeners();
        if (isActive) { backButton.onClick.AddListener(delegate { ShowWorkersMenu(false); }); }
    }
    //Cambiamos la profesión de los trabajadores
    public void SetWorkersValues(WorkerJob workerType, int amount, int maxAmount) {
        string newAmountText = amount.ToString("D2") + "/" + maxAmount.ToString("D2");
        switch (workerType) {
            case WorkerJob.UNEMPLOYED:
                unemployedAmountText.text = newAmountText;
                break;
            case WorkerJob.BUILDER:
                buildersAmountText.text = newAmountText;
                break;
            case WorkerJob.MINER:                
                minersAmountText.text = newAmountText;
                break;
            case WorkerJob.WOODCUTTER:                
                woodcuttersAmountText.text = newAmountText;
                break;
            case WorkerJob.FARMER:                
                farmersAmountText.text = newAmountText;
                break;
            case WorkerJob.WATER_MASTER:
                waterMastersAmountText.text = newAmountText;
                break;
        }
    }
    public void AddWorkerToJobListener(WorkerJob workerType) {
        if (GameManager.Instance.workerManager.ChangeJobToWorkerCheck(workerType, true)) {
            GameManager.Instance.workerManager.AssignJobToWorker(workerType);
            UpdateWorkerJobsValues(workerType, true);
        }
    }
    public void SubWorkerToJobListener(WorkerJob workerType) {
        if (!GameManager.Instance.workerManager.ChangeJobToWorkerCheck(workerType, false)) {
            GameManager.Instance.workerManager.ReleaseWorkerFromJob(workerType);
            UpdateWorkerJobsValues(workerType, false);
        }
    }
    private void UpdateWorkerJobsValues(WorkerJob workerType, bool isAdding) {
        switch (workerType) {
            case WorkerJob.UNEMPLOYED:
                //Extraemos el número actual y el máximo
                int currentUnemployed = GameManager.Instance.currentUnemployed;
                int maxUnemployed = GameManager.Instance.maxUnemployed;
                //Añadimos o restamos
                currentUnemployed = isAdding ? currentUnemployed + 1 : currentUnemployed - 1;
                //Asignamos la nueva cadena
                unemployedAmountText.text = currentUnemployed.ToString("D2") + "/" + maxUnemployed.ToString("D2");
                //Y actualizamos el manager
                GameManager.Instance.currentUnemployed = currentUnemployed;
                break;
            case WorkerJob.BUILDER:
                int currentBuilders = GameManager.Instance.currentBuilders;
                int maxBuilders = GameManager.Instance.maxBuilders;
                currentBuilders = isAdding ? currentBuilders + 1 : currentBuilders - 1;
                buildersAmountText.text = currentBuilders.ToString("D2") + "/" + maxBuilders.ToString("D2");
                GameManager.Instance.currentBuilders = currentBuilders;
                break;
            case WorkerJob.MINER:
                int currentMiners = GameManager.Instance.currentMiners;
                int maxMiners = GameManager.Instance.maxMiners;
                currentMiners = isAdding ? currentMiners + 1 : currentMiners - 1;
                minersAmountText.text = currentMiners.ToString("D2") + "/" + maxMiners.ToString("D2");
                GameManager.Instance.currentMiners = currentMiners;
                break;
            case WorkerJob.WOODCUTTER:
                int currentWoodcutters = GameManager.Instance.currentWoodcutters;
                int maxWoodcutters = GameManager.Instance.maxWoodcutters;
                currentWoodcutters = isAdding ? currentWoodcutters + 1 : currentWoodcutters - 1;
                woodcuttersAmountText.text = currentWoodcutters.ToString("D2") + "/" + maxWoodcutters.ToString("D2");
                GameManager.Instance.currentWoodcutters = currentWoodcutters;
                break;
            case WorkerJob.FARMER:
                int currentFarmers = GameManager.Instance.currentFarmers;
                int maxFarmers = GameManager.Instance.maxFarmers;
                currentFarmers = isAdding ? currentFarmers + 1 : currentFarmers - 1;
                farmersAmountText.text = currentFarmers.ToString("D2") + "/" + maxFarmers.ToString("D2");
                GameManager.Instance.currentFarmers = currentFarmers;
                break;
            case WorkerJob.WATER_MASTER:
                int currentWaterMasters = GameManager.Instance.currentWaterMasters;
                int maxWaterMasters = GameManager.Instance.maxWaterMasters;
                currentWaterMasters = isAdding ? currentWaterMasters + 1 : currentWaterMasters - 1;
                waterMastersAmountText.text = currentWaterMasters.ToString("D2") + "/" + maxWaterMasters.ToString("D2");
                GameManager.Instance.currentWaterMasters = currentWaterMasters;
                break;
        }
        //Si hemos asignado un oficio a un trabajador, se lo restamos a los desempleados, o viceversa
        if (workerType != WorkerJob.UNEMPLOYED) {
            int currentUnemployed = GameManager.Instance.currentUnemployed;
            int maxUnemployed = GameManager.Instance.maxUnemployed;
            //Añadimos o restamos
            currentUnemployed = isAdding ? currentUnemployed - 1 : currentUnemployed + 1;
            //Asignamos la nueva cadena
            unemployedAmountText.text = currentUnemployed.ToString("D2") + "/" + maxUnemployed.ToString("D2");
            //Y actualizamos el manager
            GameManager.Instance.currentUnemployed = currentUnemployed;
        }
    }
    //Click el construir un edificio
    private void PlaceBuildingListener(BuildingTypes buildingType) {
        GameManager.Instance.buildingManager.ChooseItem(buildingType);
    }
    private void PlaceTowerListener(TowerTypes towerType) {
        GameManager.Instance.buildingManager.ChooseItem(towerType);
    }
    //Click para construir una unidad
    public void PlaceUnitListener(UnitTypes unitType) {
        GameManager.Instance.unitManager.CreateNewUnit(unitType);
    }
    //Actualización de botones de construcción de edificios
    public void UpdateButtonsStatus() {
        UpdateBuildButtonsStatus();
        UpdateTowerButtonsStatus();
        UpdateUnitButtonsStatus();
    }
    public void UpdateBuildButtonsStatus() {
        int currentGold = GameManager.Instance.currentGold;
        int currentWood = GameManager.Instance.currentWood;
        int currentFood = GameManager.Instance.currentFood;
        foreach (KeyValuePair<BuildingTypes, BuildingCost> cost in GameManager.Instance.buildingManager.buildingsCostAtlas) {
            int goldCost = cost.Value.GoldCost;
            int woodCost = cost.Value.WoodCost;
            int foodCost = cost.Value.FoodCost;
            switch (cost.Key) {
                case BuildingTypes.WOOD_HOUSE:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildWoodHouseButton.enabled = true;
                    } else {
                        buildWoodHouseButton.enabled = false;
                    }
                    break;
                case BuildingTypes.STONE_HOUSE:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildStoneHouseButton.enabled = true;
                    } else {
                        buildStoneHouseButton.enabled = false;
                    }
                    break;
                case BuildingTypes.BIG_HOUSE:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildBigHouseButton.enabled = true;
                    } else {
                        buildBigHouseButton.enabled = false;
                    }
                    break;
                case BuildingTypes.MINE:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildMineButton.enabled = true;
                    } else {
                        buildMineButton.enabled = false;
                    }
                    break;
                case BuildingTypes.SAWMILL:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildSawmillButton.enabled = true;
                    } else {
                        buildSawmillButton.enabled = false;
                    }
                    break;
                case BuildingTypes.FARMHOUSE:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildFarmButton.enabled = true;
                    } else {
                        buildFarmButton.enabled = false;
                    }
                    break;
            }
        }    
    }
    public void UpdateTowerButtonsStatus() {
        int currentGold = GameManager.Instance.currentGold;
        int currentWood = GameManager.Instance.currentWood;
        int currentFood = GameManager.Instance.currentFood;
        foreach (KeyValuePair<TowerTypes, BuildingCost> cost in GameManager.Instance.buildingManager.towersCostAtlas) {
            int goldCost = cost.Value.GoldCost;
            int woodCost = cost.Value.WoodCost;
            int foodCost = cost.Value.FoodCost;
            switch (cost.Key) {
                case TowerTypes.BASE_TOWER:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildBaseTowerButton.enabled = true;
                    } else {
                        buildBaseTowerButton.enabled = false;
                    }
                    break;
                case TowerTypes.SPORE_TOWER:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildSporeTowerButton.enabled = true;
                    } else {
                        buildSporeTowerButton.enabled = false;
                    }
                    break;
                case TowerTypes.SPIKE_TOWER:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildSnowTowerButton.enabled = true;
                    } else {
                        buildSnowTowerButton.enabled = false;
                    }
                    break;
                case TowerTypes.VOLCANO_TOWER:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildVolcanoTowerButton.enabled = true;
                    } else {
                        buildVolcanoTowerButton.enabled = false;
                    }
                    break;
                case TowerTypes.SNOW_TOWER:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost) {
                        buildSnowTowerButton.enabled = true;
                    } else {
                        buildSnowTowerButton.enabled = false;
                    }
                    break;
            }
        }

    }
    public void UpdateUnitButtonsStatus() {
        int currentGold = GameManager.Instance.currentGold;
        int currentWood = GameManager.Instance.currentWood;
        int currentFood = GameManager.Instance.currentFood;
        int currentPopulation = GameManager.Instance.currentPopulation;
        int maxPopulation = GameManager.Instance.maxPopulation;
        foreach (KeyValuePair<UnitTypes, UnitCost> cost in GameManager.Instance.unitManager.unitsCostAtlas) {
            int goldCost = cost.Value.GoldCost;
            int woodCost = cost.Value.WoodCost;
            int foodCost = cost.Value.FoodCost;
            int populationCost = cost.Value.PopulationCost;
            switch (cost.Key) {
                case UnitTypes.WORKER:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost && (currentPopulation + populationCost) <= maxPopulation) {
                        createWorkerButton.enabled = true;
                    } else {
                        createWorkerButton.enabled = false;
                    }
                    break;
                case UnitTypes.WARRIOR:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost && (currentPopulation + populationCost) <= maxPopulation) {
                        createWarriorButton.enabled = true;
                    } else {
                        createWarriorButton.enabled = false;
                    }
                    break;
                case UnitTypes.DEFENDER:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost && (currentPopulation + populationCost) <= maxPopulation) {
                        createDefenderButton.enabled = true;
                    } else {
                        createDefenderButton.enabled = false;
                    }
                    break;
                case UnitTypes.GYM_ONE:
                    if (currentGold >= goldCost && currentWood >= woodCost && currentFood >= foodCost && (currentPopulation + populationCost) <= maxPopulation) {
                        createGymOneButton.enabled = true;
                    } else {
                        createGymOneButton.enabled = false;
                    }
                    break;
            }
        }

    }
    //HoverTexts
    public void SetTriggerEventTo(GameObject target, TooltipType tooltipType, string tooltipText, CostBase cost = null, ConstructionBase constructionBenefits = null) {
        //Trigger de entrada
        EventTrigger.Entry enterEventTrigger = new EventTrigger.Entry();
        enterEventTrigger.eventID = EventTriggerType.PointerEnter;
        enterEventTrigger.callback.AddListener((eventData) => { HoverInfo(true, tooltipType, tooltipText, cost, constructionBenefits); });
        //Trigger de salida
        EventTrigger.Entry exitEventTrigger = new EventTrigger.Entry();
        exitEventTrigger.eventID = EventTriggerType.PointerExit;
        exitEventTrigger.callback.AddListener((eventData) => { HoverInfo(false, tooltipType, tooltipText, cost, constructionBenefits); });
        //Trigger al click
        EventTrigger.Entry clickEventTrigger = new EventTrigger.Entry();
        clickEventTrigger.eventID = EventTriggerType.PointerClick;
        clickEventTrigger.callback.AddListener((eventData) => { HoverInfo(false, tooltipType, tooltipText, cost, constructionBenefits); });
        //Añadimos los eventos
        target.GetComponent<EventTrigger>().triggers = new List<EventTrigger.Entry> {
            enterEventTrigger,
            exitEventTrigger,
            clickEventTrigger
        };
    }
    public void SetTriggerEventTo(GameObject target, TooltipType tooltipType, string tooltipText, CostBase cost, Unit unitBenefits) {
        //Trigger de entrada
        EventTrigger.Entry enterEventTrigger = new EventTrigger.Entry();
        enterEventTrigger.eventID = EventTriggerType.PointerEnter;
        enterEventTrigger.callback.AddListener((eventData) => { HoverInfo(true, tooltipType, tooltipText, cost, unitBenefits); });
        //Trigger de salida
        EventTrigger.Entry exitEventTrigger = new EventTrigger.Entry();
        exitEventTrigger.eventID = EventTriggerType.PointerExit;
        exitEventTrigger.callback.AddListener((eventData) => { HoverInfo(false, tooltipType, tooltipText, cost, unitBenefits); });
        //Trigger al click
        EventTrigger.Entry clickEventTrigger = new EventTrigger.Entry();
        clickEventTrigger.eventID = EventTriggerType.PointerClick;
        clickEventTrigger.callback.AddListener((eventData) => { HoverInfo(false, tooltipType, tooltipText, cost, unitBenefits); });
        //Añadimos los eventos
        target.GetComponent<EventTrigger>().triggers = new List<EventTrigger.Entry> {
            enterEventTrigger,
            exitEventTrigger,
            clickEventTrigger
        };
    }
    //Seteo de la información de los tooltips
    public void HoverInfo(bool isEnabled, TooltipType tooltipType, string tooltipText, CostBase cost = null, ConstructionBase constructionBenefits = null) {
        switch (tooltipType) {
            case TooltipType.BUILDING_COST:
                costTooltip.SetActive(isEnabled);
                if (isEnabled) {
                    //Costes
                    goldBuildingCostTooltipText.text = cost.GoldCost.ToString("D2");
                    woodBuildingCostTooltipText.text = cost.WoodCost.ToString("D2");
                    foodBuildingCostTooltipText.text = cost.FoodCost.ToString("D2");
                    waterBuildingCostTooltipText.text = cost.WaterCost.ToString("D2");
                    populationBuildingCostTooltipText.text = cost.PopulationCost.ToString("D2");
                    //Beneficios
                    BuildingBase buildingBenefits = (BuildingBase) constructionBenefits;
                    populationBuildingBenefitTooltipText.text = buildingBenefits != null ? "+" + buildingBenefits.maxPopulationIncrease.ToString("D2") : "+" + 0.ToString("D2");
                    buildersBuildingBenefitTooltipText.text = buildingBenefits != null ? "+" + buildingBenefits.maxBuildersIncrease.ToString("D2") : "+" + 0.ToString("D2");
                    minersBuildingBenefitTooltipText.text = buildingBenefits != null ? "+" + buildingBenefits.maxMinersIncrease.ToString("D2") : "+" + 0.ToString("D2");
                    woodcuttersBuildingBenefitTooltipText.text = buildingBenefits != null ?"+" + buildingBenefits.maxWoodcuttersIncrease.ToString("D2") : "+" + 0.ToString("D2");
                    farmersBuildingBenefitTooltipText.text = buildingBenefits != null ?"+" + buildingBenefits.maxFarmersIncrease.ToString("D2") : "+" + 0.ToString("D2");
                    waterMastersBuildingBenefitTooltipText.text = buildingBenefits != null ?"+" + buildingBenefits.maxWaterMastersIncrease.ToString("D2") : "+" + 0.ToString("D2");
                    //Nombre
                    contentBuildingCostTooltipText.text = tooltipText;
                }                
                break;
            case TooltipType.TOWER_COST:
                towerCostTooltip.SetActive(isEnabled);
                if (isEnabled) {
                    //Costes
                    goldTowerCostTooltipText.text = cost.GoldCost.ToString("D2");
                    woodTowerCostTooltipText.text = cost.WoodCost.ToString("D2");
                    foodTowerCostTooltipText.text = cost.FoodCost.ToString("D2");
                    waterTowerCostTooltipText.text = cost.WaterCost.ToString("D2");
                    populationTowerCostTooltipText.text = cost.PopulationCost.ToString("D2");
                    //Beneficios
                    TowerBase towerBenefits = (TowerBase) constructionBenefits;
                    damageTowerBenefitTooltipText.text = towerBenefits != null ? towerBenefits.damage.ToString("D2") : 0.ToString("D2");
                    rangeTowerBenefitTooltipText.text = towerBenefits != null ? towerBenefits.range.ToString("D2") : 0.ToString("D2");
                    fireRateTowerBenefitTooltipText.text = towerBenefits != null ? towerBenefits.fireRate.ToString("D2") : 0.ToString("D2");
                    //Nombre
                    contentTowerCostTooltipText.text = tooltipText;
                }
                break;
            case TooltipType.NAME:
                contentTooltip.SetActive(isEnabled);
                if (isEnabled) {
                    contentToolTipText.text = tooltipText;
                }
                break;
        }
    }
    public void HoverInfo(bool isEnabled, TooltipType tooltipType, string tooltipText, CostBase cost, Unit unitBenefits) {
        switch (tooltipType) {
            case TooltipType.UNIT_COST:
                unitCostTooltip.SetActive(isEnabled);
                if (isEnabled) {
                    //Costes
                    goldUnitCostTooltipText.text = cost.GoldCost.ToString("D2");
                    woodUnitCostTooltipText.text = cost.WoodCost.ToString("D2");
                    foodUnitCostTooltipText.text = cost.FoodCost.ToString("D2");
                    waterUnitCostTooltipText.text = cost.WaterCost.ToString("D2");
                    populationUnitCostTooltipText.text = cost.PopulationCost.ToString("D2");
                    //Beneficios
                    damageUnitBenefitTooltipText.text = unitBenefits != null ? unitBenefits.damage.ToString("D2") : 0.ToString("D2");
                    healthUnitBenefitTooltipText.text = unitBenefits != null ? unitBenefits.health.ToString("D2") : 0.ToString("D2");
                    speedUnitBenefitTooltipText.text = unitBenefits != null ? unitBenefits.speed.ToString("D2") : 0.ToString("D2");
                    //Nombre
                    contentBuildingCostTooltipText.text = tooltipText;
                }
                break;
            case TooltipType.NAME:
                contentTooltip.SetActive(isEnabled);
                if (isEnabled) {
                    contentToolTipText.text = tooltipText;
                }
                break;
        }
    }
    //Empezar siguiente ronda
    public void StartRoundListener() {
        int currentRoundIndex = int.Parse(roundNameText.text.Split(' ')[1]);
        GameManager.Instance.spawnManager.StartNextRound(currentRoundIndex);
    }
    public void InstantiateRoundInfo(Round round) {
        for (int i = 0; i < roundInfoPanelHolder.childCount; i++) {
            Destroy(roundInfoPanelHolder.GetChild(i).gameObject);
        }
        roundNameText.text = "Round " + (round.roundID).ToString("D2");
        GameObject roundInfoPanel = Instantiate<GameObject>(roundInfoPanelPrefab, roundInfoPanelHolder);
        roundInfoPanel.GetComponent<RoundInfo>().SetInfoValues(round);
    }

}
