﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipHandler : MonoBehaviour {
    public Vector3 offset;

    void Update () {
        transform.position = Input.mousePosition + offset;
    }
}
