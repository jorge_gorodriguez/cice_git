﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public UIActionsManager uiActionsManager;
    public UIResourcesManager uiResourcesManager;

    //Singleton
    private static UIManager _instance;
    [HideInInspector] public static UIManager Instance { get { return _instance; } }
    private void Awake() { if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; } }    
}
