﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Tooltips {

    //Section names
    public const string BUILDING_MENU = "Buildings"; 
    public const string TOWERS_MENU = "Towers";
    public const string UNITS_MENU = "Units";
    public const string WORKERS_MENU = "Workers";
    public const string BACK = "Back";
    public const string NEXT_ROUND = "Next Round";

    //Building names
    public const string BUILD_HOUSE = "Build House";
    public const string BUILD_WOOD_HOUSE = "Build Wood House";
    public const string BUILD_STONE_HOUSE = "Build Stone House";
    public const string BUILD_BIG_HOUSE = "Build Big House";
    public const string BUILD_MINE = "Build Mine";
    public const string BUILD_SAWMILL = "Build Sawmill";
    public const string BUILD_FARM = "Build Farm";
    public const string BUILD_WORKSHOP = "Build Workshop";
    public const string BUILD_WATERWELL = "Build Waterwell";

    //Tower names
    public const string BUILD_BASE_TOWER = "Build Base Tower";
    public const string BUILD_SPORE_TOWER = "Build Spore Tower";
    public const string BUILD_SPIKE_TOWER = "Build Spike Tower";
    public const string BUILD_VOLCANO_TOWER = "Build Volcano Tower";
    public const string BUILD_SNOW_TOWER = "Build Snow Tower";

    //Unit names
    public const string CREATE_WORKER = "Create Worker";
    public const string CREATE_WARRIOR = "Create Warrior";
    public const string CREATE_DEFENDER = "Create Defender";
    public const string CREATE_GYM_ONE = "Create Gym One";
}
