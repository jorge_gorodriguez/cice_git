﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResourcesManager : MonoBehaviour {

    [Header("Resources Texts")]
    public Text goldText;
    public Text woodText;
    public Text foodText;
    public Text waterText;
    public Text workerText;
    public Text healthPointsText;

    //Para actualizar los textos sobre la cantidad de recursos de la parte superior del UI   
    public void UpdateResourceText(int newAmount, ResourceType resourceType) {
        switch (resourceType) {
            case ResourceType.POPULATION:
                workerText.text = newAmount.ToString("D3");
                break;
            case ResourceType.GOLD:
                goldText.text = newAmount.ToString("D3");
                break;
            case ResourceType.WOOD:
                woodText.text = newAmount.ToString("D3");
                break;
            case ResourceType.FOOD:
                foodText.text = newAmount.ToString("D3");
                break;
            case ResourceType.WATER:
                waterText.text = newAmount.ToString("D3");
                break;
        }
        GameManager.Instance.uiManager.uiActionsManager.UpdateButtonsStatus();
    }
    public void UpdateResourceText(int newAmount, int maxAmount, ResourceType resourceType) {
        switch (resourceType) {
            case ResourceType.POPULATION:
                workerText.text = newAmount.ToString("D3") + "/" + maxAmount.ToString("D3");
                break;
            case ResourceType.GOLD:
                goldText.text = newAmount.ToString("D3") + "/" + maxAmount.ToString("D3"); ;
                break;
            case ResourceType.WOOD:
                woodText.text = newAmount.ToString("D3") + "/" + maxAmount.ToString("D3");;
                break;
            case ResourceType.FOOD:
                foodText.text = newAmount.ToString("D3") + "/" + maxAmount.ToString("D3");;
                break;
            case ResourceType.WATER:
                waterText.text = newAmount.ToString("D3") + "/" + maxAmount.ToString("D3");;
                break;
        }
        GameManager.Instance.uiManager.uiActionsManager.UpdateButtonsStatus();
    }
    public void UpdateHealthPointsText(int newAmount) {
        healthPointsText.text = newAmount.ToString("D3");
    }
}
