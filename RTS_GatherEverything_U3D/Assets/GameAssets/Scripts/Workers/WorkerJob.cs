﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WorkerJob {
    UNEMPLOYED,
    BUILDER,
    MINER,
    WOODCUTTER,
    FARMER,
    WATER_MASTER
}
