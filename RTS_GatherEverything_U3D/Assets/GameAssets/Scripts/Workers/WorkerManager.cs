﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WorkerManager : MonoBehaviour {
#region VARIABLES
    List<Worker> workersList = new List<Worker>();
    List<Worker> unemployedList = new List<Worker>();
    List<Worker> buildersList = new List<Worker>();
    List<Worker> minersList = new List<Worker>();
    List<Worker> woodcuttersList = new List<Worker>();
    List<Worker> farmersList = new List<Worker>();
    List<Worker> waterMasterList = new List<Worker>();
#endregion


    //Singleton
    private static WorkerManager _instance;
    [HideInInspector] public static WorkerManager Instance { get { return _instance; } }
    private void Awake() { if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; } }

#region REGISTRO DE TRABAJADORES
    //Buscamos todos los objetos que haya en juego con el componente Worker
    public int GetAllWorkers() {
        workersList = new List<Worker>(FindObjectsOfType<Worker>());
        return workersList.Count;
    }
    //Registramos por cada oficio, la lista de trabajadores que lo desempeñan
    public void RegisterAllWorkers() {
        //Llenamos listas egún la profesión de cada uno
        foreach (Worker worker in workersList) {
            switch (worker.job) {
                case WorkerJob.UNEMPLOYED:
                    unemployedList.Add(worker);
                    GameManager.Instance.currentUnemployed++;
                    break;
                case WorkerJob.BUILDER:
                    buildersList.Add(worker);
                    GameManager.Instance.currentBuilders++;
                    break;
                case WorkerJob.MINER:
                    minersList.Add(worker);
                    GameManager.Instance.currentMiners++;
                    break;
                case WorkerJob.WOODCUTTER:
                    woodcuttersList.Add(worker);
                    GameManager.Instance.currentWoodcutters++;
                    break;
                case WorkerJob.FARMER:
                    farmersList.Add(worker);
                    GameManager.Instance.currentFarmers++;
                    break;
                case WorkerJob.WATER_MASTER:
                    waterMasterList.Add(worker);
                    GameManager.Instance.currentWaterMasters++;
                    break;
            }
        }
    }
    //Creamos un nuevo trabajador y lo añadimos a nuestra lista general de trabajadores y al diccionario en su lugar correspondiente
    public void RegisterNewWorker(Worker newWorker) {
        newWorker.GetComponent<Worker>().job = WorkerJob.UNEMPLOYED;
        //Añadimos a la lista general
        workersList.Add(newWorker.GetComponent<Worker>());
        //Y a la de desempleados
        unemployedList.Add(newWorker.GetComponent<Worker>());
        GameManager.Instance.maxUnemployed++;
        GameManager.Instance.currentUnemployed++;
        GameManager.Instance.UpdateWorkerUI();
    }
    #endregion

#region ASIGNACIÓN DE TRABAJOS
    //Comprobamos si hay hueco para asignar un nuevo oficio
    //public bool ChangeJobToWorkerCheck(WorkerJob newWorkerJob, bool isGettingAjob) {
    //    bool canGetJob = false;
    //    //Cogemos la lista de trabajadores del tipo que corresponda
    //    List<Worker> auxWorkers = new List<Worker>();
    //    switch (newWorkerJob) {
    //        case WorkerJob.UNEMPLOYED: auxWorkers = unemployedList; break;
    //        case WorkerJob.BUILDER: auxWorkers = buildersList; break;
    //        case WorkerJob.MINER: auxWorkers = minersList; break;
    //        case WorkerJob.WOODCUTTER: auxWorkers = woodcuttersList; break;
    //        case WorkerJob.FARMER: auxWorkers = farmersList; break;
    //        case WorkerJob.WATER_MASTER: auxWorkers = waterMasterList; break;
    //    }
    //    //"isGettingAJob" nos sirve para diferenciar los casos en los que  vamos a sumar un nuevo trabajador o vamos a dejarle en paro
    //    if (isGettingAjob) {
    //        //Si hay más de un trabajador desempleado
    //        if (unemployedList.Count > 0) {
    //            //Buscamos los máximos trabajadores permitidos del oficio del nuevo trabajador
    //            int maxAllowedWorkers = 0;
    //            switch (newWorkerJob) {
    //                case WorkerJob.BUILDER: maxAllowedWorkers = GameManager.Instance.maxBuilders; break;
    //                case WorkerJob.MINER: maxAllowedWorkers = GameManager.Instance.maxMiners; break;
    //                case WorkerJob.WOODCUTTER: maxAllowedWorkers = GameManager.Instance.maxWoodcutters; break;
    //                case WorkerJob.FARMER: maxAllowedWorkers = GameManager.Instance.maxFarmers; break;
    //                case WorkerJob.WATER_MASTER: maxAllowedWorkers = GameManager.Instance.maxWaterMasters; break;
    //            }
    //            //Si cabe algún trabajador más en se oficio
    //            if (auxWorkers.Count < maxAllowedWorkers) {
    //                canGetJob = true;
    //            } else {
    //                canGetJob = false;
    //            }
    //        }
    //        return canGetJob;
    //    } else { //Si vamos a quitar a un trabajo       https://www.amazon.es/Funko-POP-Bandicoot-Figura-25653/dp/B0759HS8HD/ref=sr_1_2_sspa?s=videogames&ie=UTF8&qid=1544036098&sr=1-2-spons&keywords=crash+bandicoot&psc=1     
    //        if (auxWorkers.Count > 0) { //¿Hay trabajadores para quitar?
    //            canGetJob = false;
    //        } else {
    //            canGetJob = true;
    //        }
    //        return canGetJob;
    //    }
    //}

    public bool ChangeJobToWorkerCheck(WorkerJob newWorkerJob, bool isGettingAjob) {
        //Valor para devolver
        bool canGetJob = false;
        //Buscamos la cantidad de trabajadores que actualmente están en el trabajo objetivo
        int currentWorkers = 0;
        switch (newWorkerJob) {
            case WorkerJob.BUILDER: currentWorkers = GameManager.Instance.currentBuilders; break;
            case WorkerJob.MINER: currentWorkers = GameManager.Instance.currentMiners; break;
            case WorkerJob.WOODCUTTER: currentWorkers = GameManager.Instance.currentWoodcutters; break;
            case WorkerJob.FARMER: currentWorkers = GameManager.Instance.currentFarmers; break;
            case WorkerJob.WATER_MASTER: currentWorkers = GameManager.Instance.currentWaterMasters; break;
        }
        //"isGettingAJob" nos sirve para diferenciar los casos en los que  vamos a sumar un nuevo trabajador o vamos a dejarle en paro
        if (isGettingAjob) {
            //Si hay más de un trabajador desempleado
            if (GameManager.Instance.currentUnemployed > 0) {
                //Buscamos los máximos trabajadores permitidos del oficio del nuevo trabajador
                int maxAllowedWorkers = 0;
                switch (newWorkerJob) {
                    case WorkerJob.BUILDER: maxAllowedWorkers = GameManager.Instance.maxBuilders; break;
                    case WorkerJob.MINER: maxAllowedWorkers = GameManager.Instance.maxMiners; break;
                    case WorkerJob.WOODCUTTER: maxAllowedWorkers = GameManager.Instance.maxWoodcutters; break;
                    case WorkerJob.FARMER: maxAllowedWorkers = GameManager.Instance.maxFarmers; break;
                    case WorkerJob.WATER_MASTER: maxAllowedWorkers = GameManager.Instance.maxWaterMasters; break;
                }
                //Si cabe algún trabajador más en se oficio
                if (currentWorkers < maxAllowedWorkers) {
                    canGetJob = true;
                } else {
                    canGetJob = false;
                }
            }
            return canGetJob;
        } else { //Si vamos a quitar a un trabajo       https://www.amazon.es/Funko-POP-Bandicoot-Figura-25653/dp/B0759HS8HD/ref=sr_1_2_sspa?s=videogames&ie=UTF8&qid=1544036098&sr=1-2-spons&keywords=crash+bandicoot&psc=1     
            if (currentWorkers > 0) { //¿Hay trabajadores para quitar?
                canGetJob = false;
            } else {
                canGetJob = true;
            }
            return canGetJob;
        }
    }
    //Asignamos el oficio
    public void AssignJobToWorker(WorkerJob newWorkerJob) {
        //Extraemos al último
        Worker auxWorker = unemployedList[unemployedList.Count - 1];
        unemployedList.RemoveAt(unemployedList.Count - 1);
        //Asignamos su trabajo
        auxWorker.job = newWorkerJob;
        //Cogemos la lista de trabajadores del tipo del nuevo trabajador
        List<Worker> auxWorkersList = new List<Worker>();
        switch (newWorkerJob) {
            case WorkerJob.UNEMPLOYED: auxWorkersList = unemployedList; break;
            case WorkerJob.BUILDER: auxWorkersList = buildersList; break;
            case WorkerJob.MINER: auxWorkersList = minersList; break;
            case WorkerJob.WOODCUTTER: auxWorkersList = woodcuttersList; break;
            case WorkerJob.FARMER: auxWorkersList = farmersList; break;
            case WorkerJob.WATER_MASTER: auxWorkersList = waterMasterList; break;
        }
        //Añadimos al trabajador a esta lista
        auxWorkersList.Add(auxWorker);
        //Y le damos a que busque una tarea
        auxWorker.SetWorkHat();
        AssignTaskToWorker(auxWorker);
    }
    //Quitamos el oficio
    public void ReleaseWorkerFromJob(WorkerJob oldWorkerJob) {
        //Buscamos la lista del viejo oficio
        List<Worker> auxList = new List<Worker>();
        switch (oldWorkerJob) {
            case WorkerJob.UNEMPLOYED: auxList = unemployedList; break;
            case WorkerJob.BUILDER: auxList = buildersList; break;
            case WorkerJob.MINER: auxList = minersList; break;
            case WorkerJob.WOODCUTTER: auxList = woodcuttersList; break;
            case WorkerJob.FARMER: auxList = farmersList; break;
            case WorkerJob.WATER_MASTER: auxList = waterMasterList; break;
        }
        //Y extraemos al último de la lista
        Worker auxWorker = auxList[auxList.Count - 1];
        auxList.RemoveAt(auxList.Count - 1);
        //Le dejamos en paro
        auxWorker.job = WorkerJob.UNEMPLOYED;
        auxWorker.SetWorkHat();
        //Añadimos al worker anterior la lista de desempleados
        unemployedList.Add(auxWorker);
    }        
#endregion

#region SELECCIÓN DE RECURSOS
    //Cuando un trabajador entrega sus recursos
    public void DeliverResource(Worker worker) {
        GameManager.Instance.ReceiveResourceFromWorker(worker);
        worker.assignedTask = "";
        worker.ActuallyHarvestedAmount = 0;
        AskForNewTask(worker);
    }
    //Cuando un trabajador pide una nueva tarea
    public void AskForNewTask(Worker worker) {
        StartCoroutine(WaitForNextTask(worker, 1f));
    }
    //Corrutina
    IEnumerator WaitForNextTask(Worker worker, float waitTime) {
        yield return new WaitForSeconds(waitTime);
        AssignTaskToWorker(worker);
    }

    public void PatrolUntilAsks(Worker worker) {
        StartCoroutine(PatrolAndAsk(worker, 1f));
    }

    IEnumerator PatrolAndAsk(Worker worker, float waitTime) {
        worker.GetComponent<NavMeshAgent>().SetDestination(GameManager.Instance.mapManager.RandomNavmeshLocation(worker, 1f));
        yield return new WaitForSeconds(waitTime / 2);
        worker.GetComponent<NavMeshAgent>().SetDestination(GameManager.Instance.mapManager.RandomNavmeshLocation(worker, 1f));
        yield return new WaitForSeconds(waitTime / 2);
        AskForNewTask(worker);
    }   

    //Si no tiene tarea, le buscamos una
    public void AssignTaskToWorker(Worker worker) {
        if (worker.job == WorkerJob.BUILDER) { //Si es Constructor buscamos un edificio que pueda construir
            GameObject destinationBuilding = GameManager.Instance.mapManager.LocateNewBuilding(worker);
            if (destinationBuilding != null) {
                //Enlazamos trabajador y recurso
                destinationBuilding.GetComponent<BuildingBase>().assignedWorkerId = worker.ID;
                worker.assignedTask = destinationBuilding.name;
                worker.MoveWorkerToDestination(destinationBuilding);
            } else {
                PatrolUntilAsks(worker);
            }
        } else if(worker.job != WorkerJob.UNEMPLOYED) { //Si tiene otro oficio diferente y no está en paro, le buscamos un recurso para recolectar
            GameObject destinationResource = GameManager.Instance.mapManager.LocateNewResource(worker);
            if (destinationResource != null) {
                //Enlazamos trabajador y recurso
                destinationResource.GetComponent<ResourceBase>().assignedWorkerId = worker.ID;
                worker.assignedTask = destinationResource.name;
                worker.MoveWorkerToDestination(destinationResource);
            } else {
                PatrolUntilAsks(worker);
            }
        } else { //Y a los parados, a patrullar y seguir preguntando
            PatrolUntilAsks(worker);
        }
    }
#endregion
}

