﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Worker : Unit {
    [Header("Tasks Attributes")]
    public WorkerJob job;
    public string assignedTask;
    //Cantidad de recursos que puede coger cada vez
    public int harvestCapacity;
    //Recursos que está cargando
    [SerializeField] private int actuallyHarvestedAmount;
    public int ActuallyHarvestedAmount { get { return actuallyHarvestedAmount; } set { actuallyHarvestedAmount = value; } }
    //Id único del objeto
    public int ID {
        get { return GetInstanceID(); }        
    }

    [Header("Locations")]
    public GameObject townHall; //El ayuntamiento donde entregamos recursos
    NavMeshAgent navAgent;
    [Header("Cosmetic Attributes")]
    public GameObject unemployedArm;
    public GameObject builderCostume;
    public GameObject builderArm;
    public GameObject minerCostume;
    public GameObject minerArm;
    public GameObject woodcutterCostume;
    public GameObject woodcutterArm;
    public GameObject farmerCostume;
    public GameObject farmerArm;
    public GameObject waterMasterCostume;
    public GameObject waterMasterArm;

    private void Awake() {
        navAgent = GetComponent<NavMeshAgent>();
    }
    
    //Funciones cosmeticas
    public void SetWorkerTool(bool isWorking) {
        if (isWorking) {
            GetComponent<Animator>().enabled = true;
            GetComponent<Animator>().Play("Working");
        } else {            
            GetComponent<Animator>().enabled = false;
        }
        
    }
    public void SetWorkHat() {
        unemployedArm.SetActive(false);
        builderCostume.SetActive(false);
        builderArm.SetActive(false);
        minerCostume.SetActive(false);
        minerArm.SetActive(false);
        woodcutterCostume.SetActive(false);
        woodcutterArm.SetActive(false);
        farmerCostume.SetActive(false);
        farmerArm.SetActive(false);
        waterMasterCostume.SetActive(false);
        waterMasterArm.SetActive(false);
        switch (job) {
            case WorkerJob.UNEMPLOYED:
                unemployedArm.SetActive(true);
                break;
            case WorkerJob.BUILDER:
                builderCostume.SetActive(true);
                builderArm.SetActive(true);
                break;
            case WorkerJob.MINER:
                minerCostume.SetActive(true);
                minerArm.SetActive(true);
                break;
            case WorkerJob.WOODCUTTER:
                woodcutterCostume.SetActive(true);
                woodcutterArm.SetActive(true);
                break;
            case WorkerJob.FARMER:
                farmerCostume.SetActive(true);
                farmerArm.SetActive(true);
                break;
            case WorkerJob.WATER_MASTER:
                waterMasterCostume.SetActive(true);
                waterMasterArm.SetActive(true);
                break;
        }                
    }

    //Funciones con el NavAgent para moverlo
    public void MoveWorkerToDestination(GameObject destination) {
        navAgent.SetDestination(destination.transform.position);
        navAgent.isStopped = false;
    }
    public void MoveWorkerToTownHall() {
        navAgent.SetDestination(townHall.transform.position);
        navAgent.isStopped = false;
    }

    //Manejamos los trigger
    private void OnTriggerEnter(Collider other) {
        //Si chocamos con un recurso
        if (other.tag.Contains("Resource")) {
            ResourceBase resource = other.GetComponent<ResourceBase>();
            if (resource.assignedWorkerId == ID) { //Si el recurso lo tiene asignado a si mismo y no está siendo usado ya
                StartCoroutine(HarvestAndCollect(resource));
            }
        }
        //Si chocamos con el ayutanmiento
        if (other.tag.Contains("TownHall")) {
            if (assignedTask != null) {
                if (actuallyHarvestedAmount != 0) {
                    SetWorkerTool(false);
                    GameManager.Instance.workerManager.DeliverResource(this);
                }
            } else {
                GameManager.Instance.workerManager.AskForNewTask(this);
            }
        }
        //Si chocamos con el ayutanmiento
        if (other.tag.Contains("OnProgress")) {
            BuildingOnProgress buildingOnProgress = other.GetComponent<BuildingOnProgress>();
            if (buildingOnProgress.assignedWorkerId == ID) {// && !resource.IsBeingHarvested) { //Si el recurso lo tiene asignado a si mismo y no está siendo usado ya
                StartCoroutine(Build(buildingOnProgress));//resource.GatherResources(this, harvestCapacity);
            }
        }
    }

    IEnumerator HarvestAndCollect(ResourceBase resource) {
        navAgent.isStopped = true;
        resource.GatherResources(this, harvestCapacity);
        SetWorkerTool(true);
        yield return new WaitForSeconds(resource.harvestDuration * actuallyHarvestedAmount);
        SetWorkerTool(false);
    }

    IEnumerator Build(BuildingOnProgress buildingOnProgress) {
        navAgent.isStopped = true;
        buildingOnProgress.StartBuilding(this);
        SetWorkerTool(true);
        yield return new WaitForSeconds(buildingOnProgress.buildDuration);
        SetWorkerTool(false);
    }
}

