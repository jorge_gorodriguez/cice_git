﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathDrawer : MonoBehaviour {

    public bool pathShowing;

    void Update() {
        if (pathShowing) {
            NavMeshPath currentPath = GetComponent<NavMeshAgent>().path;
            LineRenderer lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.positionCount = currentPath.corners.Length;
            for (int i = 0; i < currentPath.corners.Length; i++) {
                lineRenderer.SetPosition(i, currentPath.corners[i]);
            }
        }
    }
}
