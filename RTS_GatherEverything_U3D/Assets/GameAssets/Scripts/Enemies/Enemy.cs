﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour {

    [Header("Enemy Attributes")]
    public int damage;
    public int health;
    public float speed;
    public Sprite enemyIcon;

    float attackRate = 3;
    public bool canAttack = true;
    NavMeshAgent navMeshAgent;

    private void Awake() {
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    public void SendEnemyToGoal(Transform enemyFinish) {
        navMeshAgent.speed = speed;
        navMeshAgent.SetDestination(enemyFinish.position);
    }

    private void OnTriggerEnter(Collider other) {
        //if (other.tag == "EnemyFinish") {
        //    GameManager.Instance.LoseHealhtPoints(damage);
        //    Destroy(gameObject);
        //}
    }
    IEnumerator AttackCooldown() {
        navMeshAgent.isStopped = true;
        canAttack = false;
        yield return new WaitForSeconds(attackRate);
        navMeshAgent.isStopped = false;
        canAttack = true;
    }

    //Daño directo
    public void ReceiveDamage(int receivedDamage) {
        health -= receivedDamage;
        if (health <= 0) {
            Destroy(gameObject);
        }
    }
    public void ReceiveDamage(float receivedDamage) {
        health -= Mathf.RoundToInt(receivedDamage);
        if (health <= 0) {
            Destroy(gameObject);
        }
    }
    //Stun durante unos segundos
    public void Stun(float stunDuration) {
        StartCoroutine(StunForSeconds(stunDuration));
    }
    IEnumerator StunForSeconds(float stunDuration) {
        navMeshAgent.isStopped = true;
        yield return new WaitForSeconds(stunDuration);
        navMeshAgent.isStopped = false;
    }
    //Veneno
    public void ReceivePoisonDamage(int damage, float poisonInterval) {
        StartCoroutine(PoisonForSeconds(damage, poisonInterval));
    }
    IEnumerator PoisonForSeconds(int damage, float poisonInterval) {
        for (int i = 0; i < 10; i++) {
            ReceiveDamage(damage);
            yield return new WaitForSeconds(poisonInterval);
        }
    }
    //Daño en area
    public void ReceiveExplosionDamage(int damage, float aoeRange) {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, aoeRange);        
        foreach (Collider collider in hitColliders) {
            if (collider.tag == "Enemy") {
                collider.GetComponent<Enemy>().ReceiveDamage(damage);
            }
        }
    }
    //Slow en area
    public void ReceiveSlowExplosion(float slowPercent, float slowDuration, float aoeRange) {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, aoeRange);
        foreach (Collider collider in hitColliders) {
            if (collider.tag == "Enemy") {
                collider.GetComponent<Enemy>().SlowMovement(slowPercent, slowDuration);
            }
        }
    }
    public void SlowMovement(float slowPercent, float slowDuration) {
        StartCoroutine(SlowMovementOverTime(slowPercent, slowDuration));
    }
    IEnumerator SlowMovementOverTime(float slowPercent, float slowDuration) {
        float normalSpeed = navMeshAgent.speed;
        navMeshAgent.speed = normalSpeed - normalSpeed * slowPercent;
        yield return new WaitForSeconds(slowDuration);
        navMeshAgent.speed = normalSpeed;
    }

}
