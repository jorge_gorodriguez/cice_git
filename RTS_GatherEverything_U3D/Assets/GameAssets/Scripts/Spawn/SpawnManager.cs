﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnManager : MonoBehaviour {

    [Header("Spawn intervals")]
    public float betweenUnitsTime;
    public float betweenGroupsTime;

    [Header("Rounds List")]
    public List<Round> rounds;

    [Header("Locations")]
    public Transform enemyStart;
    public Transform enemyFinish;

    CoroutineQueue queue;

    //Singleton
    private static SpawnManager _instance;
    [HideInInspector] public static SpawnManager Instance { get { return _instance; } }
    private void Awake() { if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; } }
       
    //Arranca la cola con las rondas encoladas
    public void StartRounds() {
        queue.StartLoop();
    }
    //Encolamos las rondas y un tiempo de espera entre ellas
    public void EnqueueRounds() {
        queue = new CoroutineQueue(this);
        foreach (Round round in rounds) {
            queue.EnqueueAction(InstantiateRound(round));
            queue.EnqueueWait(betweenGroupsTime);
        }
    }
    //Empezamos la siguiente ronda
    public void StartNextRound(int currentRound) {
        if (currentRound < rounds.Count) {
            StartCoroutine(InstantiateRound(rounds[currentRound]));
            GameManager.Instance.UpdateRoundsUI(rounds[currentRound]);
        }        
    }    
    IEnumerator InstantiateRound(Round round) {
        for (int i = 0; i < round.spawnedAmount; i++) {
            yield return new WaitForSeconds(betweenUnitsTime);
            Enemy spawnedEnemy = SpawnEnemy(round.spawnedEnemy);
            spawnedEnemy.SendEnemyToGoal(enemyFinish);
        }
    }
    //Spawneamos un enemigo y emparentamos
    public Enemy SpawnEnemy(Enemy enemy) {
        enemy.gameObject.SetActive(true);
        return Instantiate<Enemy>(enemy, enemyStart);
    }

}
