﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoundInfo : MonoBehaviour {
       
    public Image monsterImage;
    public Text monsterAmount;
    public Text healthText;
    public Text damageText;
    public Text speedText;

    internal void SetInfoValues(Round round) {
        monsterImage.sprite = round.spawnedEnemy.enemyIcon;
        monsterAmount.text = round.spawnedAmount.ToString("D2");
        healthText.text = round.spawnedEnemy.GetComponent<Enemy>().health.ToString("D2");
        damageText.text = round.spawnedEnemy.GetComponent<Enemy>().damage.ToString("D2");
        speedText.text = ((int)round.spawnedEnemy.GetComponent<Enemy>().speed).ToString("D2");
    }
}
