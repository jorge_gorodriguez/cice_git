﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Round {

    public int roundID;
    public Enemy spawnedEnemy;
    public int spawnedAmount;
    //[SerializeField] public Round nestedRound;

    public Round() {
        this.roundID = 0;
        this.spawnedEnemy = null;
        this.spawnedAmount = 0;
        //this.nestedRound = null;
    }

    public Round(int roundID, Enemy spawnedEnemy, int spawnedAmount/*, Round nestedRound*/) {
        this.roundID = roundID;
        this.spawnedEnemy = spawnedEnemy;
        this.spawnedAmount = spawnedAmount;
        //this.nestedRound = nestedRound;
    }

}
