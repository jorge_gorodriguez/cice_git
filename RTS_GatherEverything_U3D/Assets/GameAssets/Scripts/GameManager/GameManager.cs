﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    #region VARIABLES
    [Header("Health Points")]
    public int currentHealthPoints;

    [Header("Resources")]
    public int currentGold;
    public int currentWood;
    public int currentFood;
    public int currentWater;

    [Header("Population & Workers")]
    public int currentPopulation;
    public int maxPopulation;
    public int currentUnemployed;
    public int maxUnemployed;
    public int currentBuilders;
    public int maxBuilders;
    public int currentMiners;
    public int maxMiners;
    public int currentWoodcutters;
    public int maxWoodcutters;
    public int currentFarmers;
    public int maxFarmers; 
    public int currentWaterMasters;
    public int maxWaterMasters;

    [Header("Managers")]
    public BuildingManager buildingManager;
    public MapManager mapManager;
    public WorkerManager workerManager;
    public UIManager uiManager;
    public UnitManager unitManager;
    public SpawnManager spawnManager;
    #endregion
    //Singleton
    private static GameManager _instance;
    [HideInInspector] public static GameManager Instance { get { return _instance; } }
    private void Awake() { if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; } DontDestroyOnLoad(gameObject); SetInitialValues(); }

    //Inicialización de todos los managers
    public void SetInitialValues() {
        //Trabajadores        
        currentPopulation = workerManager.GetAllWorkers();
        maxUnemployed = currentPopulation;
        workerManager.RegisterAllWorkers();
        //Recursos
        mapManager.CreateResourcesAndBuildingsAtlas();
        UpdateResourcesUI();
        //Oficios
        UpdateWorkerUI();
        //Edificios
        buildingManager.InitializeBuildingPrices();
        buildingManager.InitializeTowerPrices();
        //Unidades
        unitManager.InitializeUnitsPrices();
        //UI        
        uiManager.uiActionsManager.AssignMenuButtonsListeners();
    }
    //Funciones para actualizar la parte superior de la UI
    public void UpdateResourcesUI() {
        uiManager.uiResourcesManager.UpdateResourceText(currentGold, ResourceType.GOLD);
        uiManager.uiResourcesManager.UpdateResourceText(currentWood, ResourceType.WOOD);
        uiManager.uiResourcesManager.UpdateResourceText(currentFood, ResourceType.FOOD);
        uiManager.uiResourcesManager.UpdateResourceText(currentWater, ResourceType.WATER);
        uiManager.uiResourcesManager.UpdateResourceText(currentPopulation, maxPopulation, ResourceType.POPULATION);
        uiManager.uiResourcesManager.UpdateHealthPointsText(currentHealthPoints);
    }
    //Funciones para actualizar el panel de trabajadores
    public void UpdateWorkerUI() {
        uiManager.uiActionsManager.SetWorkersValues(WorkerJob.UNEMPLOYED, currentUnemployed, maxUnemployed);
        uiManager.uiActionsManager.SetWorkersValues(WorkerJob.BUILDER, currentBuilders, maxBuilders);
        uiManager.uiActionsManager.SetWorkersValues(WorkerJob.MINER, currentMiners, maxMiners);
        uiManager.uiActionsManager.SetWorkersValues(WorkerJob.WOODCUTTER, currentWoodcutters, maxWoodcutters);
        uiManager.uiActionsManager.SetWorkersValues(WorkerJob.FARMER, currentFarmers, maxFarmers);
        uiManager.uiActionsManager.SetWorkersValues(WorkerJob.WATER_MASTER, currentWaterMasters, maxWaterMasters);
    }
    //Actualizamos el panel de Información sobre las rondas
    public void UpdateRoundsUI(Round round) {
        uiManager.uiActionsManager.InstantiateRoundInfo(round);
    }
    //Recibimos recursos de un trabajador
    public void ReceiveResourceFromWorker(Worker worker) {        
        switch (worker.job) {
            case WorkerJob.MINER:
                currentGold += worker.ActuallyHarvestedAmount;
                break;
            case WorkerJob.WOODCUTTER:
                currentWood += worker.ActuallyHarvestedAmount;
                break;
            case WorkerJob.FARMER:
                currentFood += worker.ActuallyHarvestedAmount;
                break;
            case WorkerJob.WATER_MASTER:
                currentWater += worker.ActuallyHarvestedAmount;
                break;
        }
        UpdateResourcesUI();
    }
    //Pasamos costes de edificios o unidades que se acaben de construir
    public void PayBuilding(BuildingCost buildingCost) {
        currentGold -= buildingCost.GoldCost;
        currentWood -= buildingCost.WoodCost;
        currentFood -= buildingCost.FoodCost;
        currentWater -= buildingCost.WaterCost;
        UpdateResourcesUI();
    }
    public void PayUnit(UnitCost unitCost) {
        currentPopulation += unitCost.PopulationCost;
        currentGold -= unitCost.GoldCost;
        currentWood -= unitCost.WoodCost;
        currentFood -= unitCost.FoodCost;
        currentWater -= unitCost.WaterCost;
        UpdateResourcesUI();
    }
    //Control sobre la vida
    public void GainHealhtPoints(int damage = 1) {
        currentHealthPoints += damage;
        uiManager.uiResourcesManager.UpdateHealthPointsText(currentHealthPoints);
    }
    public void LoseHealhtPoints(int damage) {
        currentHealthPoints -= damage;        
        uiManager.uiResourcesManager.UpdateHealthPointsText(currentHealthPoints);
        if (currentHealthPoints <= 0) {
            EndGame();    
        }
    }
    //Mandar a la escena de puntuación
    public void EndGame() {
        Score score = new Score(currentGold, currentWood, currentFood, currentWater, currentHealthPoints,
                                currentUnemployed, currentBuilders, currentMiners, currentWoodcutters, currentFarmers, currentWaterMasters);
        score.StoreJson("lastScore", JsonUtility.ToJson(score));
        SceneManager.LoadScene("endMenu");
    }

    public void SetGameSpeed(int gameSpeed) {
        Time.timeScale = gameSpeed;
    }
}
