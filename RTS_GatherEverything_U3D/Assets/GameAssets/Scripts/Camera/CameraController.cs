﻿
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float panSpeed = 20f; //Velocidad de movimiento de la cámara
    public float panBorderThickness = 10f; //Marca la distancia a la que empezará a moverse
    public Vector2 panLimit; //Limite horizontal y vertical que la cámara podrá moverse respecto al origen
    public float scrollSpeed = 20f; //Velocidad del zoom
    public float minZoom = 15f; //Zoom mínimo
    public float maxZoom = 60f ; //Zoom máximo

	void Update () {
        Vector3 newPosition = transform.parent.position;
        float newCameraSize = Camera.main.orthographicSize;

        //Capturamos los Inputs de teclado y de la posición del raton
        if (Input.GetKey(KeyCode.W) || Input.mousePosition.y >= Screen.height - panBorderThickness) {
            newPosition.x -= panSpeed * Time.deltaTime;
            newPosition.z += panSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S) || Input.mousePosition.y <= panBorderThickness) {
            newPosition.x += panSpeed * Time.deltaTime;
            newPosition.z -= panSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D) || Input.mousePosition.x >= Screen.width - panBorderThickness) {
            newPosition.x += panSpeed * Time.deltaTime;
            newPosition.z += panSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A) || Input.mousePosition.x <= panBorderThickness) {
            newPosition.x -= panSpeed * Time.deltaTime;
            newPosition.z -= panSpeed * Time.deltaTime;
        }


        //Cogemos la entrada de la rueda del ratón, para el zoom
        float scroll = Input.GetAxis("Mouse ScrollWheel");
        newCameraSize -= scroll * scrollSpeed * 100f * Time.deltaTime;

        //Controlamos que ni X ni Y ni Z se salen de sus limites
        //newPosition.x = Mathf.Clamp(newPosition.x, -panLimit.x, panLimit.x);
        newCameraSize = Mathf.Clamp(newCameraSize, minZoom, maxZoom);
        //newPosition.z = Mathf.Clamp(newPosition.z, -panLimit.y, panLimit.y);

        //Asignamos la nueva posición
        transform.parent.position = newPosition;
        Camera.main.orthographicSize = newCameraSize;

    }
}
