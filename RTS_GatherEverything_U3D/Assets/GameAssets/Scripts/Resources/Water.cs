﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Water : ResourceBase {

    Vector3 colliderStep;
    Vector3 scaleStep;
    Vector3 initialScale;
    Vector3 initialCollider;
    void Start() {
        IsBeingHarvested = false;
        CurrentResourceAmount = MaxResourceAmount;
        resourceType = ResourceType.WATER;
        scaleStep = transform.localScale / MaxResourceAmount;
        colliderStep = GetComponent<BoxCollider>().size / MaxResourceAmount;
        initialScale = transform.localScale;
        initialCollider = GetComponent<BoxCollider>().size;
    }

    public override void GatherResources(Worker worker, int times) {
        //Comprobamos cuantas veces realmente vamos a recoger recursos, y hacemos disminuir en consonancia el recurso
        if (CurrentResourceAmount == 0) {
            StartCoroutine(GrowNewResources(worker, MaxResourceAmount));
        } else {
            int newTimes = GatherActualResources(worker, times);
            StartCoroutine(HarvestResource(scaleStep, colliderStep, newTimes, worker));
        }
    }

    //Calculamos la cantidad de recursos que realmente vamos a recoger y lo mandamos de vuelta
    int GatherActualResources(Worker worker, int times) {
        worker.ActuallyHarvestedAmount = 0;
        for (int i = 0; i < times; i++) {
            if (CurrentResourceAmount > 0) {
                CurrentResourceAmount--;
                worker.ActuallyHarvestedAmount++;
            } else if (CurrentResourceAmount <= 0) {
                break;
            }
        }
        return worker.ActuallyHarvestedAmount;
    }

    IEnumerator GrowNewResources(Worker worker, int times) {
        //Bloqueamos el estado del objeto y el movimiento del trabajador
        worker.GetComponent<NavMeshAgent>().isStopped = true;
        IsBeingHarvested = true;
        //Creamos un nuevo padre para este objeto
        GameObject auxParent = new GameObject();
        auxParent.transform.parent = transform.parent;
        auxParent.transform.position = transform.position;
        auxParent.transform.localScale = Vector3.one;
        transform.parent = auxParent.transform;

        //Cogemos la escala inicial del objeto
        Vector3 startingLocalScale = transform.localScale;
        Vector3 targetLocalScale = initialScale;

        //Y la de su collider
        BoxCollider boxCollider = transform.GetComponent<BoxCollider>();
        auxParent.AddComponent<BoxCollider>();
        auxParent.GetComponent<BoxCollider>().size = boxCollider.size;
        auxParent.GetComponent<BoxCollider>().center = boxCollider.center;  

        //Y la posición inicial del padre
        Vector3 positionStep = new Vector3(0f, scaleStep.y / 2, 0f);
        Vector3 startingLocalPosition = auxParent.transform.localPosition;
        Vector3 targetLocalPosition = startingLocalPosition - positionStep * times;

        //Hacemos recrecer el agua
        float elapsedTime = 0;
        while (elapsedTime < harvestDuration * maxResourceAmount) {
            elapsedTime += Time.deltaTime;
            float blend = Mathf.Clamp01(elapsedTime / (harvestDuration * maxResourceAmount));

            //Aumentamos poco a poco la escala del objeto
            Vector3 localScale = transform.localScale;
            localScale.y = Mathf.Lerp(startingLocalScale.y, targetLocalScale.y, blend);
            transform.localScale = localScale;

            //Aumentamos poco a poco la posición del objeto padre
            Vector3 localPosition = auxParent.transform.localPosition;
            localPosition.x = Mathf.Lerp(startingLocalPosition.x, targetLocalPosition.x, blend);
            localPosition.y = Mathf.Lerp(startingLocalPosition.y, targetLocalPosition.y, blend);
            localPosition.z = Mathf.Lerp(startingLocalPosition.z, targetLocalPosition.z, blend);
            auxParent.transform.localPosition = localPosition;
            yield return null;
        }
        yield return new WaitForSeconds(harvestDuration * maxResourceAmount);
        CurrentResourceAmount = MaxResourceAmount;
        transform.parent = auxParent.transform.parent;
        boxCollider.size = auxParent.GetComponent<BoxCollider>().size;
        boxCollider.center = auxParent.GetComponent<BoxCollider>().center;
        Destroy(auxParent);
        int newTimes = GatherActualResources(worker, worker.harvestCapacity);
        StartCoroutine(HarvestResource(scaleStep, colliderStep, newTimes, worker));
    }

    IEnumerator HarvestResource(Vector3 scaleStep, Vector3 colliderStep, int times, Worker worker) {
        //Bloqueamos el estado del objeto
        IsBeingHarvested = true;
        //Creamos un nuevo padre para este objeto
        GameObject auxParent = new GameObject();
        auxParent.transform.parent = transform.parent;
        auxParent.transform.position = transform.position;
        auxParent.transform.localScale = Vector3.one;
        transform.parent = auxParent.transform;

        //Cogemos la escala inicial del objeto
        Vector3 startingLocalScale = transform.localScale;
        Vector3 targetLocalScale = startingLocalScale - scaleStep * times;

        //Y la de su collider
        BoxCollider boxCollider = transform.GetComponent<BoxCollider>();
        auxParent.AddComponent<BoxCollider>();
        auxParent.GetComponent<BoxCollider>().size = boxCollider.size;
        auxParent.GetComponent<BoxCollider>().center = boxCollider.center;

        //Y la posición inicial del padre
        Vector3 positionStep = new Vector3(0f, scaleStep.y / 2, 0f);
        Vector3 startingLocalPosition = auxParent.transform.localPosition;
        Vector3 targetLocalPosition = startingLocalPosition + positionStep * times;

        float elapsedTime = 0;
        while (elapsedTime < harvestDuration * times) {
            elapsedTime += Time.deltaTime;
            float blend = Mathf.Clamp01(elapsedTime / (harvestDuration * times));

            //Disminuimos poco a poco la escala del objeto
            Vector3 localScale = transform.localScale;
            localScale.y = Mathf.Lerp(startingLocalScale.y, targetLocalScale.y, blend) <= 0.000001f ? 0.000001f : Mathf.Lerp(startingLocalScale.y, targetLocalScale.y, blend);
            transform.localScale = localScale;

            //Bajamos poco a poco la posición del objeto padre
            Vector3 localPosition = auxParent.transform.localPosition;
            localPosition.y = Mathf.Lerp(startingLocalPosition.y, targetLocalPosition.y, blend);
            auxParent.transform.localPosition = localPosition;
            yield return null;
        }        
        yield return new WaitForEndOfFrame();
        worker.MoveWorkerToTownHall();
        transform.parent = auxParent.transform.parent;
        boxCollider.size = auxParent.GetComponent<BoxCollider>().size;
        boxCollider.center = auxParent.GetComponent<BoxCollider>().center;
        Destroy(auxParent);           
        assignedWorkerId = -1;
        IsBeingHarvested = false;
    }
}
