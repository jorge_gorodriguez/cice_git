﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ResourceType {
    POPULATION,
    GOLD,
    WOOD,
    FOOD,
    WATER,
    NONE
}
