﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Farm : ResourceBase {

    //Lista de prefabs para ir plantando
    public List<GameObject> foodPrefabs;
    //La casa a la que pertenece este cultivo
    public GameObject FarmWarehouse { get { return transform.parent.gameObject; } }
    //List de objetos plantados
    List<GameObject> plantedFood = new List<GameObject>();
    //Variables para controlar las distancias
    float centerToFoodCenterDistance;
    float foodCenterToFoodCenterDistance;
    int rowsCount;
    float halfSize;
    float padding;
    float spacing;
    float objectSize;

    //Delegado para utilizarlo como callback
    delegate int AfterGatherDelegate(Worker worker, int times);
    AfterGatherDelegate afterGatherDelegate;

    void Start () {
        CurrentResourceAmount = 0;
        SetPaddingAndSpacingValues();
        afterGatherDelegate = GatherActualResources;
        resourceType = ResourceType.FOOD;
    }
    //Ajustamos ciertos valores de padding, spacing, el tamaño del objeto en ese reparto, etc.
    public void SetPaddingAndSpacingValues() {
        //           |padding + spacing + obj + spacing + spacing + obj + spacing| x  ,  y  |spacing + obj + spacing + spacing + obj + spacing + padding
        //           <--------------------------0.5------------------------------>
        //-0.5f, 0.5f|-----------------------------------------------------------|0.0f, 0.5f|-----------------------------------------------------------|0.5f, 0.5f
        rowsCount = (int)Mathf.Sqrt(MaxResourceAmount); //Número de filas y columnas
        halfSize = 1.25f; //Sabemos que los bounds nos dan +-0.5f en las esquinas, da igual la escala
        padding = halfSize * 10 / 100; //10% padding
        objectSize = (halfSize - padding) / (rowsCount / 2); //Sabemos cuanto ocuparía cada objeto tras el padding, rellenando el hueco
        spacing = padding / 2; //El spacing será la mitad del padding
        objectSize -= spacing; //Y se lo restamos al tamaño del objeto

        centerToFoodCenterDistance = spacing / 2 + objectSize / 2;
        foodCenterToFoodCenterDistance = spacing + objectSize;
    }
    //Punto de entrada para el trabajador, según haya recursos o no le tocará plantarlos
    public override void GatherResources(Worker worker, int times) {
        if (CurrentResourceAmount == 0) {
            StartCoroutine(GrowNewResources(worker, times, afterGatherDelegate));
        } else {
            StartCoroutine(HarvestResources(worker, times, afterGatherDelegate));
        }
    }

    //Calculamos la cantidad de recursos que realmente vamos a recoger y lo mandamos de vuelta
    int GatherActualResources(Worker worker, int times) {        
        worker.ActuallyHarvestedAmount = 0;
        for (int i = 0; i < times; i++) {
            if (CurrentResourceAmount > 0) {
                CurrentResourceAmount--;
                RetrieveResource();
                worker.ActuallyHarvestedAmount++;
            } else if (CurrentResourceAmount <= 0) {
                break;
            }
        }
        return worker.ActuallyHarvestedAmount;
    }

    //Eliminamos un recurso tanto de la lista como del terreno
    private void RetrieveResource() {
        GameObject retrievedFood = plantedFood[plantedFood.Count - 1];
        plantedFood.RemoveAt(plantedFood.Count - 1);
        Destroy(retrievedFood);
    }

    IEnumerator GrowNewResources(Worker worker, int times, AfterGatherDelegate afterGatherDelegate) {
        //Bloqueamos el estado del objeto y el movimiento del trabajador
        worker.GetComponent<NavMeshAgent>().isStopped = true;
        IsBeingHarvested = true;
        //Plantamos la comida
        for (int i = 0; i < rowsCount; i++) {
            for (int j = 0; j < rowsCount; j++) {
                yield return new WaitForSeconds(harvestDuration);
                PlantAtRowColumn(j, i);                
            }
        }
        CurrentResourceAmount = MaxResourceAmount;
        afterGatherDelegate(worker, times);
        assignedWorkerId = -1;
        worker.MoveWorkerToTownHall();
        IsBeingHarvested = false;
    }

    //X Posición en la fila, o Columna
    //Y Posición en la columna, o Fila
    void PlantAtRowColumn(int x, int y) {        
        float halfRow = ((float)rowsCount - 1) / 2;
        int stepsX = Mathf.FloorToInt(Mathf.Abs(halfRow - x));
        int stepsY = Mathf.FloorToInt(Mathf.Abs(halfRow - y));
        float horizontalPosition = centerToFoodCenterDistance + foodCenterToFoodCenterDistance * stepsX;
        float verticalPosition = centerToFoodCenterDistance + foodCenterToFoodCenterDistance * stepsY;
        horizontalPosition = x < rowsCount / 2 ? horizontalPosition * -1 : horizontalPosition;
        verticalPosition = y >= rowsCount / 2 ? verticalPosition * -1 : verticalPosition;
        Vector3 newPosition = new Vector3(horizontalPosition, 0.6f, verticalPosition);
        Vector3 newSize = new Vector3(objectSize, objectSize, objectSize); 
        GameObject newFood = Instantiate<GameObject>(foodPrefabs[UnityEngine.Random.Range(0, foodPrefabs.Count)], transform);
        newFood.transform.localPosition = newPosition;
        newFood.transform.localScale = newSize;
        plantedFood.Add(newFood);
    }

    IEnumerator HarvestResources(Worker worker, int times, AfterGatherDelegate gatherAfterPlantDelegate) { 
        IsBeingHarvested = true;
        yield return new WaitForSeconds(harvestDuration * gatherAfterPlantDelegate(worker, times));
        assignedWorkerId = -1;
        IsBeingHarvested = false;
        worker.MoveWorkerToTownHall();
    }
}
