﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ResourceBase : MonoBehaviour {

    [HideInInspector] public ResourceType resourceType;
    public int maxResourceAmount;
    public int MaxResourceAmount { get { return maxResourceAmount; } set { maxResourceAmount = value; } }
    [SerializeField] int currentResourceAmount;
    public int CurrentResourceAmount { get { return currentResourceAmount; } set { currentResourceAmount = value; } }


    [SerializeField] private bool isBeingHarvested;
    public bool IsBeingHarvested { get { return isBeingHarvested; } set { isBeingHarvested = value; } }
    public int assignedWorkerId = -1;

    public float harvestDuration = 2;

    public abstract void GatherResources(Worker worker, int times);
}
