﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Wood : ResourceBase {
    Vector3 colliderStep;
    Vector3 scaleStep;
    void Start() {
        IsBeingHarvested = false;
        CurrentResourceAmount = MaxResourceAmount;
        resourceType = ResourceType.WOOD;
        scaleStep = transform.localScale / MaxResourceAmount;
        colliderStep = GetComponent<BoxCollider>().size / MaxResourceAmount;
    }

    public override void GatherResources(Worker worker, int times) {
        //Comprobamos cuantas veces realmente vamos a recoger recursos, y hacemos disminuir en consonancia el recurso
        int newTimes = GatherActualResources(worker, times);
        StartCoroutine(HarvestResource(scaleStep, colliderStep, newTimes, worker));
    }

    //Calculamos la cantidad de recursos que realmente vamos a recoger y lo mandamos de vuelta
    int GatherActualResources(Worker worker, int times) {
        worker.ActuallyHarvestedAmount = 0;
        for (int i = 0; i < times; i++) {
            if (CurrentResourceAmount > 0) {
                CurrentResourceAmount--;
                worker.ActuallyHarvestedAmount++;
            } else if (CurrentResourceAmount <= 0) {
                break;
            }
        }
        return worker.ActuallyHarvestedAmount;
    }

    IEnumerator HarvestResource(Vector3 scaleStep, Vector3 colliderStep, int times, Worker worker) {
        //Bloqueamos el estado del objeto
        IsBeingHarvested = true;
        //Creamos un nuevo padre para este objeto
        GameObject auxParent = new GameObject();
        auxParent.transform.parent = transform.parent;
        auxParent.transform.position = transform.position;
        auxParent.transform.localScale = Vector3.one;
        transform.parent = auxParent.transform;

        //Cogemos la escala inicial del objeto
        Vector3 startingLocalScale = transform.localScale;
        Vector3 targetLocalScale = startingLocalScale - scaleStep * times;

        //Y la de su collider
        BoxCollider boxCollider = transform.GetComponent<BoxCollider>();
        Vector3 startingColliderSize = boxCollider.size;
        Vector3 targetColliderSize = startingColliderSize + colliderStep * times * 2;

        //Y la posición inicial del padre
        Vector3 positionStep = new Vector3(0f, scaleStep.y / 2, 0f);
        Vector3 startingLocalPosition = auxParent.transform.localPosition;
        Vector3 targetLocalPosition = startingLocalPosition - positionStep * times;

        float elapsedTime = 0;
        while (elapsedTime < harvestDuration * times) {
            elapsedTime += Time.deltaTime;
            float blend = Mathf.Clamp01(elapsedTime / (harvestDuration * times));

            //Disminuimos poco a poco la escala del objeto
            Vector3 localScale = transform.localScale;
            localScale.x = Mathf.Lerp(startingLocalScale.x, targetLocalScale.x, blend);
            localScale.y = Mathf.Lerp(startingLocalScale.y, targetLocalScale.y, blend);
            localScale.z = Mathf.Lerp(startingLocalScale.z, targetLocalScale.z, blend);
            transform.localScale = localScale;

            //Aumentamos el collider en la misma cantidad
            Vector3 localColliderScale = boxCollider.size;
            localColliderScale.x = Mathf.Lerp(startingColliderSize.x, targetColliderSize.x, blend);
            localColliderScale.y = Mathf.Lerp(startingColliderSize.y, targetColliderSize.y, blend);
            localColliderScale.z = Mathf.Lerp(startingColliderSize.z, targetColliderSize.z, blend);
            boxCollider.size = localColliderScale;

            //Bajamos poco a poco la posición del objeto padre
            Vector3 localPosition = auxParent.transform.localPosition;
            localPosition.x = Mathf.Lerp(startingLocalPosition.x, targetLocalPosition.x, blend);
            localPosition.y = Mathf.Lerp(startingLocalPosition.y, targetLocalPosition.y, blend);
            localPosition.z = Mathf.Lerp(startingLocalPosition.z, targetLocalPosition.z, blend);
            auxParent.transform.localPosition = localPosition;
            yield return null;
        }
        if (CurrentResourceAmount <= 0) {
            yield return new WaitForEndOfFrame();
            worker.MoveWorkerToTownHall();
            Destroy(auxParent);
            MapManager.Instance.DeleteWastedResource(this);
        } else {
            yield return new WaitForEndOfFrame();
            worker.MoveWorkerToTownHall();
            IsBeingHarvested = false;
            assignedWorkerId = -1;
            transform.parent = auxParent.transform.parent;
            Destroy(auxParent);
        }
    }
}
