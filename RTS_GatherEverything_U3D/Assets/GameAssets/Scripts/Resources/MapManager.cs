﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MapManager : MonoBehaviour {    

    //Lista de recursos
    public List<Gold> goldVeins = new List<Gold>();
    public List<Wood> forests = new List<Wood>();
    public List<Farm> farms = new List<Farm>();
    public List<Water> waters = new List<Water>();
    //Listas de edificios
    public List<BuildingOnProgress> buildingsOnProgress = new List<BuildingOnProgress>();
    public List<House> houses = new List<House>();
    public List<Mine> mines = new List<Mine>();
    public List<Sawmill> sawmills = new List<Sawmill>();
    public List<Farmhouse> farmhouses = new List<Farmhouse>();
    public List<Waterwell> waterwells = new List<Waterwell>();
    public List<Workshop> workshops = new List<Workshop>();
    //Lista de torres
    public List<BaseTower> baseTowers = new List<BaseTower>();
    public List<SporeTower> sporeTowers = new List<SporeTower>();
    public List<SpikeTower> spikeTowers = new List<SpikeTower>();
    public List<VolcanoTower> volcanoTowers = new List<VolcanoTower>();
    public List<SnowTower> snowTowers = new List<SnowTower>();

    //Singleton
    private static MapManager _instance;
    [HideInInspector] public static MapManager Instance { get { return _instance; } }
    private void Awake() { if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; } }
    
    //Actualizamos nuestras listas de recursos con lo que haya en el mapa inicialmente
    public void CreateResourcesAndBuildingsAtlas(){
        goldVeins = new List<Gold>(FindObjectsOfType<Gold>());
        forests = new List<Wood>(FindObjectsOfType<Wood>());
        farms = new List<Farm>(FindObjectsOfType<Farm>());
        waters = new List<Water>(FindObjectsOfType<Water>());
        //Edificios
        buildingsOnProgress = new List<BuildingOnProgress>(FindObjectsOfType<BuildingOnProgress>());
        houses = new List<House>(FindObjectsOfType<House>());
        mines = new List<Mine>(FindObjectsOfType<Mine>());
        sawmills = new List<Sawmill>(FindObjectsOfType<Sawmill>());
        waterwells = new List<Waterwell>(FindObjectsOfType<Waterwell>());
        workshops = new List<Workshop>(FindObjectsOfType<Workshop>());

    }
    //Registramos un nuevo recurso
    public void RegisterNewResource(ResourceBase newResource) {
        switch (newResource.resourceType) {            
            case ResourceType.GOLD:
                goldVeins.Add((Gold)newResource);
                break;
            case ResourceType.WOOD:
                forests.Add((Wood)newResource);
                break;
            case ResourceType.FOOD:
                farms.Add((Farm)newResource);
                break;
            case ResourceType.WATER:
                waters.Add((Water)newResource);
                break;
        }
    }    
    //Registramos un nuevo recurso
    public void DeleteWastedResource(ResourceBase wastedResource) {
        switch (wastedResource.resourceType) {
            case ResourceType.GOLD:
                goldVeins.Remove((Gold)wastedResource);
                break;
            case ResourceType.WOOD:
                forests.Remove((Wood)wastedResource);
                break;
            case ResourceType.FOOD:
                farms.Remove((Farm)wastedResource);
                break;
        }
        Destroy(wastedResource.gameObject);
    }    
    //Obtenemos un nuevo recurso según el tipo de trabajador
    public GameObject LocateNewResource(Worker worker) {
        List<ResourceBase> resourcesList = new List<ResourceBase>();
        switch (worker.job) {
            case WorkerJob.MINER:
                foreach (Gold goldVein in goldVeins) {
                    resourcesList.Add(goldVein);
                }
                break;
            case WorkerJob.WOODCUTTER:
                foreach (Wood tree in forests) {
                    resourcesList.Add(tree);
                }
                break;
            case WorkerJob.FARMER:
                foreach (Farm farm in farms) {
                    resourcesList.Add(farm);
                }
                break;
            case WorkerJob.WATER_MASTER:
                foreach (Water water in waters) {
                    resourcesList.Add(water);
                }
                break;
        }
        return GetClosestResource(worker.gameObject, resourcesList);
    }    
    //Buscamos el recurso más cercano
    public GameObject GetClosestResource(GameObject origin, List<ResourceBase> resourcesList) {
        GameObject destinationResource = null;
        float distanceToResource = 0f;
        foreach (ResourceBase resource in resourcesList) {
            float auxDistanceToResource = (resource.transform.position - origin.transform.position).magnitude;
            if (distanceToResource == 0f) { //En la primera vuelta la distancia será 0, pero la guardamos
                if (!resource.IsBeingHarvested) { //Comprobamos si está siendo utilizado por alguien
                    if (resource.assignedWorkerId == -1) { //Y si no está asignado a nadie
                        distanceToResource = auxDistanceToResource;
                        destinationResource = resource.gameObject;
                    }
                }
            } else { //Las demás vueltas
                if (auxDistanceToResource < distanceToResource) { //Si la distancia es menor a la distancia anterior...
                    if (!resource.GetComponent<ResourceBase>().IsBeingHarvested) { //Comprobamos si está siendo utilizado por alguien
                        if (resource.assignedWorkerId == -1) { //Y si no está asignado a nadie
                            distanceToResource = auxDistanceToResource;
                            destinationResource = resource.gameObject;
                        }
                    }
                }
            }
        }
        return destinationResource;
    }
    //Registramos un edificio en construcción
    public void RegisterNewBuilding(BuildingBase newBuilding) {
        switch (newBuilding.buildingType) {
            case BuildingTypes.HOUSE:
                houses.Add((House)newBuilding);
                break;
            case BuildingTypes.WOOD_HOUSE:
                houses.Add((House)newBuilding);
                break;
            case BuildingTypes.STONE_HOUSE:
                houses.Add((House)newBuilding);
                break;
            case BuildingTypes.BIG_HOUSE:
                houses.Add((House)newBuilding);
                break;
            case BuildingTypes.WORKSHOP:
                workshops.Add((Workshop)newBuilding);
                break;
            case BuildingTypes.MINE:
                mines.Add((Mine)newBuilding);
                break;
            case BuildingTypes.SAWMILL:
                sawmills.Add((Sawmill)newBuilding);
                break;
            case BuildingTypes.FARMHOUSE:
                farmhouses.Add((Farmhouse)newBuilding);
                break;
            case BuildingTypes.WATERWELL:
                waterwells.Add((Waterwell)newBuilding);
                break;
            case BuildingTypes.ON_PROGRESS:
                buildingsOnProgress.Add((BuildingOnProgress)newBuilding);
                break;
        }        
    }
    public void RegisterNewTower(TowerBase newTower) {
        switch (newTower.towerType) {
            case TowerTypes.BASE_TOWER:
                baseTowers.Add((BaseTower)newTower);
                break;
            case TowerTypes.SPORE_TOWER:
                sporeTowers.Add((SporeTower)newTower);
                break;
            case TowerTypes.SPIKE_TOWER:
                spikeTowers.Add((SpikeTower)newTower);
                break;
            case TowerTypes.VOLCANO_TOWER:
                volcanoTowers.Add((VolcanoTower)newTower);
                break;
            case TowerTypes.SNOW_TOWER:
                snowTowers.Add((SnowTower)newTower);
                break;
        }
    }
    //Lo sacamos de la lista al construirlo
    public void DeleteCompletedBuilding(BuildingBase oldBuilding) {
        switch (oldBuilding.buildingType) {
            case BuildingTypes.HOUSE:
                houses.Remove((House)oldBuilding);
                break;
            case BuildingTypes.WOOD_HOUSE:
                houses.Remove((House)oldBuilding);
                break;
            case BuildingTypes.STONE_HOUSE:
                houses.Remove((House)oldBuilding);
                break;
            case BuildingTypes.BIG_HOUSE:
                houses.Remove((House)oldBuilding);
                break;
            case BuildingTypes.MINE:
                mines.Remove((Mine)oldBuilding);
                break;
            case BuildingTypes.SAWMILL:
                sawmills.Remove((Sawmill)oldBuilding);
                break;
            case BuildingTypes.FARMHOUSE:
                break;
            case BuildingTypes.TOWN_HAWLL:
                break;
            case BuildingTypes.ON_PROGRESS:
                buildingsOnProgress.Remove((BuildingOnProgress)oldBuilding);
                break;
        }
    }
    //Obtenemos un nuevo recurso según el tipo de trabajador
    public GameObject LocateNewBuilding(Worker worker) {
        List<BuildingBase> buildingsList = new List<BuildingBase>();
        switch (worker.job) {
            case WorkerJob.UNEMPLOYED:
                foreach (House house in houses) {
                    buildingsList.Add(house);
                }
                break;
            case WorkerJob.MINER:
                foreach (Mine mine in mines) {
                    buildingsList.Add(mine);
                }
                break;
            case WorkerJob.WOODCUTTER:
                foreach (Sawmill sawmill in sawmills) {
                    buildingsList.Add(sawmill);
                }
                break;
            case WorkerJob.FARMER:
                break;
            case WorkerJob.BUILDER:
                foreach (BuildingOnProgress buildingOnProgress in buildingsOnProgress) {
                    buildingsList.Add(buildingOnProgress);
                }
                break;
        }
        return GetClosesBuilding(worker.gameObject, buildingsList);
    }
    //Buscamos el edificio más cercano
    public GameObject GetClosesBuilding(GameObject origin, List<BuildingBase> buildingList) {
        GameObject destinationBuilding = null;
        float distanceToBuilding = 0f;
        foreach (BuildingBase building in buildingList) {
            float auxDistanceToBuilding = (building.transform.position - origin.transform.position).magnitude;
            if (distanceToBuilding == 0f) { //En la primera vuelta la distancia será 0, pero la guardamos
                if (!building.IsBeingBuilt) { //Comprobamos si está siendo utilizado por alguien
                    if (building.assignedWorkerId == -1) { //Y si no está asignado a nadie
                        distanceToBuilding = auxDistanceToBuilding;
                        destinationBuilding = building.gameObject;
                    }
                }
            } else { //Las demás vueltas
                if (auxDistanceToBuilding < distanceToBuilding) { //Si la distancia es menor a la distancia anterior...
                    if (!building.IsBeingBuilt) { //Comprobamos si está siendo utilizado por alguien
                        if (building.assignedWorkerId == -1) { //Y si no está asignado a nadie
                            distanceToBuilding = auxDistanceToBuilding;
                            destinationBuilding = building.gameObject;
                        }
                    }
                }
            }
        }
        return destinationBuilding;
    }
    //Posición aleatoria en el NavMesh
    public Vector3 RandomNavmeshLocation(Worker worker, float radius) {
        //Cogemos una dirección aleatoria dentro de una esfera
        Vector3 randomDirection = UnityEngine.Random.insideUnitSphere * radius;
        //Y le sumamos la posición del trabajador
        randomDirection += worker.transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        //Calculamos una posición que golpee en dirección aleatoria al NavMesh
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1)) {
            finalPosition = hit.position;
        }
        return finalPosition;
    }
}
