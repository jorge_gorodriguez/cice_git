﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingOnProgress : BuildingBase {

    public GameObject nextBuilding;

    public void RegisterBuildingOnProgress(GameObject nextBuilding) {
        this.nextBuilding = nextBuilding;
        this.buildDuration = nextBuilding.GetComponent<ConstructionBase>().buildDuration;
        GameManager.Instance.mapManager.RegisterNewBuilding(this);
    }

    public void StartBuilding(Worker worker) {
        StartCoroutine(Build(worker));
    }

    IEnumerator Build(Worker worker) {
        GetComponent<Animator>().enabled = true;
        GetComponent<Animator>().Play("BuildOnProgress");
        yield return new WaitForSeconds(buildDuration);
        GameObject newBuilding = Instantiate<GameObject>(nextBuilding, transform.parent);
        Vector3 buildingPosition = new Vector3(transform.position.x, 0f, transform.position.z);
        newBuilding.transform.position = buildingPosition;
        newBuilding.GetComponent<ConstructionBase>().OnBuiltFinish(worker);
        GameManager.Instance.mapManager.DeleteCompletedBuilding(this);        
        Destroy(this.gameObject);
    }
}
