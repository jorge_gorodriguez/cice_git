﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingBase : ConstructionBase {
    [Header("Building attributes")]
    public BuildingTypes buildingType;    
    //Beneficios al construir el edificio
    public int maxPopulationIncrease;
    public int maxMinersIncrease;
    public int maxWoodcuttersIncrease;
    public int maxFarmersIncrease;
    public int maxWaterMastersIncrease;
    public int maxBuildersIncrease;

    //Al terminar la construcción de un edificio
    public override void OnBuiltFinish(Worker worker) {        
        GameManager.Instance.maxPopulation += maxPopulationIncrease;
        //GameManager.Instance.maxUnemployed += maxPopulationIncrease;
        GameManager.Instance.maxMiners += maxMinersIncrease;
        GameManager.Instance.maxWoodcutters+= maxWoodcuttersIncrease;
        GameManager.Instance.maxFarmers += maxFarmersIncrease;
        GameManager.Instance.maxWaterMasters += maxWaterMastersIncrease;
        GameManager.Instance.maxBuilders += maxBuildersIncrease;
        GameManager.Instance.UpdateWorkerUI();
        GameManager.Instance.UpdateResourcesUI();
        //Y mandamos al constructor a buscar nuevas tareas
        GameManager.Instance.workerManager.AskForNewTask(worker);
    }
}
