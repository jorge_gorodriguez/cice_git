﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapToGrid : MonoBehaviour {

    public float gridSize;

    public Vector3 ReturnNearestGridPoint(Vector3 currentPoint) {
        Vector3 nearestPosition;
        nearestPosition.x = Mathf.Floor(currentPoint.x / gridSize) * gridSize;
        nearestPosition.y = Mathf.Floor(currentPoint.y / gridSize) * gridSize;
        nearestPosition.z = Mathf.Floor(currentPoint.z / gridSize) * gridSize;
        return nearestPosition;
    }

}
