﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceableBuilding : MonoBehaviour {

    [HideInInspector] public List<Collider> otherColliders = new List<Collider>();

    private void OnTriggerEnter(Collider other) {
        if (other.tag.Contains("Building")) {
            otherColliders.Add(other);
        }
        if (other.tag.Contains("Resource")) {
            if (this.GetComponent<BuildingBase>()) { //Si es un edificio (no una torre)
                if (this.GetComponent<BuildingBase>().buildingType != BuildingTypes.FARMHOUSE) { //Y si no es una granja
                    otherColliders.Add(other);
                } else { //Si eres una granja
                    if (other.tag != "Resource_Farm") { //Y el recurso que tocas no es parte de tu propia granja
                        otherColliders.Add(other);
                    }
                }
            } else {
                otherColliders.Add(other);
            }
        }
        if (other.tag.Contains("Tower")) {
            otherColliders.Add(other);
        }
        if (other.tag.Contains("Wall")) {
            otherColliders.Add(other);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag.Contains("Building")) {
            otherColliders.Remove(other);
        }
        if (other.tag.Contains("Resource")) {
            otherColliders.Remove(other);
        }
        if (other.tag.Contains("Tower")) {
            otherColliders.Remove(other);
        }
        if (other.tag.Contains("Wall")) {
            otherColliders.Remove(other);
        }
    }
}
