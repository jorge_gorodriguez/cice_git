﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingManager : MonoBehaviour {
    bool hasPlaced;


    //Máscara para detectar la capa suelo para torres y edificios
    [Header("Detection Layers")]
    public LayerMask buildingsGround;
    public LayerMask towersGround;
    //Diferenciamos entre edificio y torre, para las diferentes capas
    public bool isABuilding;
    //Objeto que vamos a construir e irá pegado al ratón
    Transform currentBuilding;
    //Referencia al objeto que realmente vamos a construir, sin modificar
    GameObject auxBuilding;
    //Materiales con un shader para cambiar la apariencia del edificio que se va a construir antes de hacerlo
    [Header("Materials with shaders")]
    public Material isValidPositionMaterial;
    public Material isInvalidPositionMaterial;
    //Grid para ajustar las posiciones al construir
    SnapToGrid grid;
    //Script que tienen las construcciones para comprobar si la posición en la que están está vacia
    PlaceableBuilding placeableBuilding;
    //Prefabs para "mientras" se construye un edificio
    [Header("Buildings Prefab")]
    public GameObject buildOnProgressPrefab;
    //Prefabs de edificios para construir
    public GameObject woodHousePrefab;
    public GameObject stoneHousePrefab;
    public GameObject bigHousePrefab;
    public GameObject minePrefab;
    public GameObject sawmillPrefab;
    public GameObject farmPrefab;
    public GameObject workshopPrefab;
    public GameObject waterwellPrefab;
    //Prefabs de torres para construir
    [Header("Towers Prefab")]
    public GameObject baseTowerPrefab;
    public GameObject sporeTowerPrefab;
    public GameObject spikeTowerPrefab;
    public GameObject volcanoTowerPrefab;
    public GameObject snowTowerPrefab;

    //Diccionario con los tipos de edificios y su coste asociado
    public Dictionary<BuildingTypes, BuildingCost> buildingsCostAtlas = new Dictionary<BuildingTypes, BuildingCost>();
    public Dictionary<TowerTypes, BuildingCost> towersCostAtlas = new Dictionary<TowerTypes, BuildingCost>();

    //Singleton
    private static BuildingManager _instance;
    [HideInInspector] public static BuildingManager Instance { get { return _instance; } }
    private void Awake() {
        if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; }
        grid = GetComponent<SnapToGrid>();
    }    
    void Update () {
        if (currentBuilding != null && !hasPlaced) {
            SetBuildingPosition();            
            if (Input.GetMouseButtonDown(0)) {                
                PlaceBuilding();                
            }
            if (Input.GetKeyDown(KeyCode.LeftShift)) {
                RotateBuilding();                
            }
            if (Input.GetMouseButtonDown(1)) {
                CancelPlacement();
            }
        }        
	}
    //Inicialización del atlas de precios
    public void InitializeBuildingPrices() {
        buildingsCostAtlas = new Dictionary<BuildingTypes, BuildingCost>();
        buildingsCostAtlas.Add(BuildingTypes.WOOD_HOUSE, new BuildingCost(25, 50, 0, 0));
        buildingsCostAtlas.Add(BuildingTypes.STONE_HOUSE, new BuildingCost(50, 100, 0, 25));
        buildingsCostAtlas.Add(BuildingTypes.BIG_HOUSE, new BuildingCost(75, 150, 25, 50));
        buildingsCostAtlas.Add(BuildingTypes.MINE, new BuildingCost(100, 50, 0, 0));
        buildingsCostAtlas.Add(BuildingTypes.SAWMILL, new BuildingCost(50, 100, 0, 0));
        buildingsCostAtlas.Add(BuildingTypes.FARMHOUSE, new BuildingCost(50, 50, 0, 50));
        buildingsCostAtlas.Add(BuildingTypes.WATERWELL, new BuildingCost(0, 50, 0, 100));
        buildingsCostAtlas.Add(BuildingTypes.WORKSHOP, new BuildingCost(25, 25, 25, 25));
    }
    public void InitializeTowerPrices() {
        towersCostAtlas = new Dictionary<TowerTypes, BuildingCost>();
        towersCostAtlas.Add(TowerTypes.BASE_TOWER, new BuildingCost(50, 50, 10, 10));
        towersCostAtlas.Add(TowerTypes.SPORE_TOWER, new BuildingCost(75, 75, 25, 20));
        towersCostAtlas.Add(TowerTypes.SPIKE_TOWER, new BuildingCost(25, 50, 10, 10));
        towersCostAtlas.Add(TowerTypes.VOLCANO_TOWER, new BuildingCost(50, 75, 100, 0));
        towersCostAtlas.Add(TowerTypes.SNOW_TOWER, new BuildingCost(25, 50, 0, 25));
    }
    //Hace que nustro edificio seleccionado acompañe al ratón
    public void SetBuildingPosition() {
        Ray rayToMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit mouseHitInfo;
        LayerMask currentLayer = isABuilding ? buildingsGround : towersGround;
        if (Physics.Raycast(rayToMouse, out mouseHitInfo, Mathf.Infinity, currentLayer)) {
            Vector3 newPosition = grid.ReturnNearestGridPoint(new Vector3(mouseHitInfo.point.x, 0f, mouseHitInfo.point.z));
            newPosition = new Vector3(newPosition.x, 0f, newPosition.z);
            currentBuilding.position = newPosition;
        }
        if (IsEmptyPosition()) {
            //foreach (MeshRenderer renderer in currentBuilding.GetComponentsInChildren<MeshRenderer>()) {
            //    renderer.material = isValidPositionMaterial;
            //}
            currentBuilding.GetComponentInChildren<MeshRenderer>().material = isValidPositionMaterial;
        } else {
            //foreach (MeshRenderer renderer in currentBuilding.GetComponentsInChildren<MeshRenderer>()) {
            //    renderer.material = isInvalidPositionMaterial;
            //}
            currentBuilding.GetComponentInChildren<MeshRenderer>().material = isInvalidPositionMaterial;
        }
    }
    //Cambiamos el edificio que vamos a construir por uno que simula ser una obra
    //Y le pedimos que tras X segundos, construya el edificio que queríamos
    public void PlaceBuilding() {
        if (IsEmptyPosition()) {
            hasPlaced = true;
            Transform buildOnProgress = Instantiate<GameObject>(buildOnProgressPrefab, transform).transform;
            buildOnProgress.position = currentBuilding.position;
            buildOnProgress.localScale = currentBuilding.localScale;
            auxBuilding.transform.rotation = currentBuilding.rotation;
            buildOnProgress.GetComponent<BuildingOnProgress>().RegisterBuildingOnProgress(auxBuilding);
            if (isABuilding) {
                GameManager.Instance.PayBuilding(buildingsCostAtlas[currentBuilding.GetComponent<BuildingBase>().buildingType]);
            } else {
                GameManager.Instance.PayBuilding(towersCostAtlas[currentBuilding.GetComponent<TowerBase>().towerType]);
            }
            Destroy(currentBuilding.gameObject);
        }
    }
    //Comprobamos si la posición donde vamos a construir está vacia
    bool IsEmptyPosition() {
        if (placeableBuilding.otherColliders.Count > 0) {
            return false;
        }
        return true;
    }
    //Rotamos 90º en el eje Y, para colocar nuestro edificio
    private void RotateBuilding() {
        currentBuilding.Rotate(Vector3.up, 90f);
    }
    //Cancelamos la selección actual
    public void CancelPlacement() {
        Destroy(currentBuilding.gameObject);
    }
    //Llamado desde UIManager, instancia una copia del edificio que vamos a construir
    public void ChooseItem(BuildingTypes buildingType) {
        isABuilding = true;
        currentBuilding = null;
        hasPlaced = false;
        auxBuilding = null;
        switch (buildingType) {
            case BuildingTypes.WOOD_HOUSE:
                auxBuilding = woodHousePrefab;
                currentBuilding = Instantiate<GameObject>(woodHousePrefab, transform).transform;
                break;
            case BuildingTypes.STONE_HOUSE:
                auxBuilding = woodHousePrefab;
                currentBuilding = Instantiate<GameObject>(stoneHousePrefab, transform).transform;
                break;
            case BuildingTypes.BIG_HOUSE:
                auxBuilding = woodHousePrefab;
                currentBuilding = Instantiate<GameObject>(bigHousePrefab, transform).transform;
                break;
            case BuildingTypes.MINE:
                auxBuilding = minePrefab;
                currentBuilding = Instantiate<GameObject>(minePrefab, transform).transform;
                break;
            case BuildingTypes.SAWMILL:
                auxBuilding = sawmillPrefab;
                currentBuilding = Instantiate<GameObject>(sawmillPrefab, transform).transform;
                break;
            case BuildingTypes.FARMHOUSE:
                auxBuilding = farmPrefab;
                currentBuilding = Instantiate<GameObject>(farmPrefab, transform).transform;
                break;
            case BuildingTypes.WORKSHOP:
                auxBuilding = workshopPrefab;
                currentBuilding = Instantiate<GameObject>(workshopPrefab, transform).transform;
                break;
            case BuildingTypes.WATERWELL:
                auxBuilding = waterwellPrefab;
                currentBuilding = Instantiate<GameObject>(waterwellPrefab, transform).transform;
                break;
        }
        placeableBuilding = currentBuilding.gameObject.AddComponent<PlaceableBuilding>();
        currentBuilding.GetComponentInChildren<MeshRenderer>().material = isValidPositionMaterial;
    }

    public void ChooseItem(TowerTypes towerType) {
        isABuilding = false;
        currentBuilding = null;
        hasPlaced = false;
        auxBuilding = null;
        switch (towerType) {
            case TowerTypes.BASE_TOWER:
                auxBuilding = baseTowerPrefab;
                currentBuilding = Instantiate<GameObject>(baseTowerPrefab, transform).transform;
                break;
            case TowerTypes.SPORE_TOWER:
                auxBuilding = sporeTowerPrefab;
                currentBuilding = Instantiate<GameObject>(sporeTowerPrefab, transform).transform;
                break;
            case TowerTypes.SPIKE_TOWER:
                auxBuilding = spikeTowerPrefab;
                currentBuilding = Instantiate<GameObject>(spikeTowerPrefab, transform).transform;
                break;
            case TowerTypes.VOLCANO_TOWER:
                auxBuilding = volcanoTowerPrefab;
                currentBuilding = Instantiate<GameObject>(volcanoTowerPrefab, transform).transform;
                break;
            case TowerTypes.SNOW_TOWER:
                auxBuilding = snowTowerPrefab;
                currentBuilding = Instantiate<GameObject>(snowTowerPrefab, transform).transform;
                break;
        }
        placeableBuilding = currentBuilding.gameObject.AddComponent<PlaceableBuilding>();
        currentBuilding.GetComponentInChildren<MeshRenderer>().material = isValidPositionMaterial;
    }
} 
