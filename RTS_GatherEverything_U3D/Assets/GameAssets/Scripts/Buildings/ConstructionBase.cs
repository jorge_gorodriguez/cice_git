﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ConstructionBase : MonoBehaviour {
    [Header("Construction Attributes")]
    [SerializeField] private bool isBeingBuilt;
    public bool IsBeingBuilt { get { return isBeingBuilt; } set { isBeingBuilt = value; } }
    public int assignedWorkerId = -1;
    public float buildDuration;

    public abstract void OnBuiltFinish(Worker worker);

}
