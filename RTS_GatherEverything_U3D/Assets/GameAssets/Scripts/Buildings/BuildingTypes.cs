﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum BuildingTypes {
    HOUSE,
    WOOD_HOUSE,
    STONE_HOUSE,
    BIG_HOUSE,
    WORKSHOP,
    MINE,
    SAWMILL,
    WATERWELL,
    FARMHOUSE,
    TOWN_HAWLL,
    ON_PROGRESS
}

public enum TowerTypes {
    BASE_TOWER,
    SPORE_TOWER,
    SPIKE_TOWER,
    VOLCANO_TOWER,
    SNOW_TOWER
}

public enum UnitTypes {
    WORKER,
    WARRIOR,
    DEFENDER,
    GYM_ONE
}