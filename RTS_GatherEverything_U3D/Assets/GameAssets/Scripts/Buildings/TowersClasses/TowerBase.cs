﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackPriority {
    CLOSEST,
    FURTHEST,
    MOST_HEALTH_POINTS,
    LESS_HEALTH_POINTS,
    FASTEST,
    SLOWEST,
    STRONGEST,
    WEAKEST
}

public enum AttackType {
    BASIC,
    POISON,
    AOE,
    PRECISION,
    SLOW
}

public abstract class TowerBase : ConstructionBase {
    [Header("Tower attributes")]
    public TowerTypes towerType;
    public AttackType attackType;
    public AttackPriority attackPriority;
    public GameObject projectilePrefab;
    public GameObject projectileParticle;
    public Transform projectileHolder;
    public int damage;
    public int fireRate;
    public int range;
    public bool canAttack = true;

    private void Update() {
        if (canAttack) { //Si podemos atacar
            //La torre busca alrededor suya en cierto rango
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, range);
            List<Enemy> enemiesAtRange = new List<Enemy>();
            foreach (Collider collider in hitColliders) {
                if (collider.tag == "Enemy") {
                    //Cogemos los enemigos
                    enemiesAtRange.Add(collider.GetComponent<Enemy>());
                }
            }
            //Según la prioridad de ataque elegida, cogemos un único target y lo atacamos
            if (enemiesAtRange.Count > 0) {
                switch (attackPriority) {
                    case AttackPriority.CLOSEST:
                        Attack(GetClosestEnemy(enemiesAtRange));
                        break;
                    case AttackPriority.FURTHEST:
                        Attack(GetFastestEnemy(enemiesAtRange));
                        break;
                    case AttackPriority.MOST_HEALTH_POINTS:
                        Attack(GetMostHealthEnemy(enemiesAtRange));
                        break;
                    case AttackPriority.LESS_HEALTH_POINTS:
                        Attack(GetLeastHealthEnemy(enemiesAtRange));
                        break;
                    case AttackPriority.FASTEST:
                        Attack(GetFastestEnemy(enemiesAtRange));
                        break;
                    case AttackPriority.SLOWEST:
                        Attack(GetSlowestEnemy(enemiesAtRange));
                        break;
                    case AttackPriority.STRONGEST:
                        Attack(GetStrongestEnemy(enemiesAtRange));
                        break;
                    case AttackPriority.WEAKEST:
                        Attack(GetWeakestEnemy(enemiesAtRange));
                        break;
                }
            }
        }
    }

    public void Attack(GameObject target) {
        LaunchProjectile(target.GetComponent<Enemy>());
        StartCoroutine(AttackCooldown());
    }

    public void LaunchProjectile(Enemy targetEnemy) {
        GameObject projectile = Instantiate<GameObject>(projectilePrefab, projectileHolder);
        projectile.GetComponent<ProjectileBase>().SetTargetAndDamage(targetEnemy, this);
    }

    //private void OnDrawGizmos() {
    //    Gizmos.DrawWireSphere(transform.position, range);
    //}
    //Al terminar la construcción de un edificio
    public override void  OnBuiltFinish(Worker worker) {        
        //Y mandamos al constructor a buscar nuevas tareas
        GameManager.Instance.workerManager.AskForNewTask(worker);
    }
    //Criterios de busqueda para la selección del enemigo a atacar
    public GameObject GetClosestEnemy(List<Enemy> enemies) {
        float distanceToEnemy = 0;
        GameObject targetEnemy = null;
        foreach (Enemy enemy in enemies) {
            float auxdistanceToEnemy = Mathf.Abs((enemy.transform.position - transform.position).magnitude);
            if (distanceToEnemy == 0f) {
                distanceToEnemy = auxdistanceToEnemy;
                targetEnemy = enemy.gameObject;
            } else {
                if (auxdistanceToEnemy < distanceToEnemy) {
                    distanceToEnemy = auxdistanceToEnemy;
                    targetEnemy = enemy.gameObject;
                }
            }
        }
        return targetEnemy;
    }
    public GameObject GetFurthestEnemy(List<Enemy> enemies) {
        float distanceToEnemy = 0;
        GameObject targetEnemy = null;
        foreach (Enemy enemy in enemies) {
            float auxdistanceToEnemy = Mathf.Abs((enemy.transform.position - transform.position).magnitude);
            if (distanceToEnemy == 0f) {
                distanceToEnemy = auxdistanceToEnemy;
                targetEnemy = enemy.gameObject;
            } else {
                if (auxdistanceToEnemy > distanceToEnemy) {
                    distanceToEnemy = auxdistanceToEnemy;
                    targetEnemy = enemy.gameObject;
                }
            }
        }
        return targetEnemy;
    }
    public GameObject GetMostHealthEnemy(List<Enemy> enemies) {
        float enemyHealth = 0;
        GameObject targetEnemy = null;
        foreach (Enemy enemy in enemies) {
            float auxEnemyHealth = enemy.health;
            if (enemyHealth == 0f) {
                enemyHealth = auxEnemyHealth;
                targetEnemy = enemy.gameObject;
            } else {
                if (auxEnemyHealth > enemyHealth) {
                    enemyHealth = auxEnemyHealth;
                    targetEnemy = enemy.gameObject;
                }
            }
        }
        return targetEnemy;
    }
    public GameObject GetLeastHealthEnemy(List<Enemy> enemies) {
        float enemyHealth = 0;
        GameObject targetEnemy = null;
        foreach (Enemy enemy in enemies) {
            float auxEnemyHealth = enemy.health;
            if (enemyHealth == 0f) {
                enemyHealth = auxEnemyHealth;
                targetEnemy = enemy.gameObject;
            } else {
                if (auxEnemyHealth < enemyHealth) {
                    enemyHealth = auxEnemyHealth;
                    targetEnemy = enemy.gameObject;
                }
            }
        }
        return targetEnemy;
    }
    public GameObject GetFastestEnemy(List<Enemy> enemies) {
        float enemySpeed = 0;
        GameObject targetEnemy = null;
        foreach (Enemy enemy in enemies) {
            float auxEnemySpeed = enemy.speed;
            if (enemySpeed == 0f) {
                enemySpeed = auxEnemySpeed;
                targetEnemy = enemy.gameObject;
            } else {
                if (auxEnemySpeed > enemySpeed) {
                    enemySpeed = auxEnemySpeed;
                    targetEnemy = enemy.gameObject;
                }
            }
        }
        return targetEnemy;
    }
    public GameObject GetSlowestEnemy(List<Enemy> enemies) {
        float enemySpeed = 0;
        GameObject targetEnemy = null;
        foreach (Enemy enemy in enemies) {
            float auxEnemySpeed = enemy.speed;
            if (enemySpeed == 0f) {
                enemySpeed = auxEnemySpeed;
                targetEnemy = enemy.gameObject;
            } else {
                if (auxEnemySpeed < enemySpeed) {
                    enemySpeed = auxEnemySpeed;
                    targetEnemy = enemy.gameObject;
                }
            }
        }
        return targetEnemy;
    }
    public GameObject GetStrongestEnemy(List<Enemy> enemies) {
        float enemyDamage = 0;
        GameObject targetEnemy = null;
        foreach (Enemy enemy in enemies) {
            float auxEnemyDamage = enemy.damage;
            if (enemyDamage == 0f) {
                enemyDamage = auxEnemyDamage;
                targetEnemy = enemy.gameObject;
            } else {
                if (auxEnemyDamage > enemyDamage) {
                    enemyDamage = auxEnemyDamage;
                    targetEnemy = enemy.gameObject;
                }
            }
        }
        return targetEnemy;
    }
    public GameObject GetWeakestEnemy(List<Enemy> enemies) {
        float enemyDamage = 0;
        GameObject targetEnemy = null;
        foreach (Enemy enemy in enemies) {
            float auxEnemyDamage = enemy.damage;
            if (enemyDamage == 0f) {
                enemyDamage = auxEnemyDamage;
                targetEnemy = enemy.gameObject;
            } else {
                if (auxEnemyDamage < enemyDamage) {
                    enemyDamage = auxEnemyDamage;
                    targetEnemy = enemy.gameObject;
                }
            }
        }
        return targetEnemy;
    }
    //Cooldown entre ataques
    public IEnumerator AttackCooldown() {
        canAttack = false;
        yield return new WaitForSeconds(fireRate);
        canAttack = true;
    }
}
