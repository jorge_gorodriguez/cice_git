﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowTower : TowerBase {
    public float aoeRange;
    public float slowPercent;
    public float slowDuration;
}
