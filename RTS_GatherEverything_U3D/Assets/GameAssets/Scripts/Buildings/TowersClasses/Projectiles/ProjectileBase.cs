﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBase : MonoBehaviour {
    
    TowerBase projectileOriginTower;
    Transform targetEnemy;
    public float projectileSpeed;
    float elapsedDistance = 0;

    public void SetTargetAndDamage(Enemy enemy, TowerBase towerBase) {
        projectileOriginTower = towerBase;
        targetEnemy = enemy.transform;        
    }

    private void Update() {
        if (targetEnemy == null) { //Puede ser que mientras vamos de camino al enemigo este muera o sea destruido por otra torre
            Destroy(gameObject);
            return;
        }
        Vector3 directionToEnemy = targetEnemy.position - transform.position;
        float distanceThisFrame = projectileSpeed * Time.deltaTime;
        //La distancia que avanzamos en este frame
        if (directionToEnemy.magnitude <= distanceThisFrame) { //Hemos tocado algo
            ApplyDamageAndEffects(targetEnemy.GetComponent<Enemy>());
            ApplyParticleEffects(targetEnemy);
            return;
        }
        elapsedDistance += distanceThisFrame;
        transform.Translate(directionToEnemy.normalized * distanceThisFrame, Space.World); //Aplicamos el movimiento
        transform.LookAt(targetEnemy, Vector3.up);
    }

    public void ApplyDamageAndEffects(Enemy enemy) {
        switch (projectileOriginTower.attackType) {
            case AttackType.BASIC:
                enemy.ReceiveDamage(projectileOriginTower.damage);
                enemy.Stun(((BaseTower)projectileOriginTower).stunTime);
                break;
            case AttackType.POISON:
                enemy.ReceiveDamage(projectileOriginTower.damage);
                enemy.ReceivePoisonDamage(((SporeTower)projectileOriginTower).poisonDamage, ((SporeTower)projectileOriginTower).poisonInterval);
                break;
            case AttackType.AOE:
                enemy.ReceiveExplosionDamage(projectileOriginTower.damage, ((VolcanoTower)projectileOriginTower).aoeRange); 
                break;
            case AttackType.PRECISION:
                enemy.ReceiveDamage(projectileOriginTower.damage + projectileOriginTower.damage * ((SpikeTower)projectileOriginTower).damageIncrease * elapsedDistance/10); //Daño aumentado en un tanto por cierto según lo recorrido por el proyectil
                break;
            case AttackType.SLOW:
                enemy.ReceiveExplosionDamage(projectileOriginTower.damage, ((SnowTower)projectileOriginTower).aoeRange);
                enemy.ReceiveSlowExplosion(((SnowTower)projectileOriginTower).slowPercent, ((SnowTower)projectileOriginTower).slowDuration, ((SnowTower)projectileOriginTower).aoeRange); //Daño aumentado en un tanto por cierto según lo recorrido por el proyectil
                break;
        }
        Destroy(gameObject);
    }
    public void ApplyParticleEffects(Transform targetEnemy) {
        GameObject particle = Instantiate<GameObject>(projectileOriginTower.projectileParticle, transform);
        particle.transform.localPosition = Vector3.zero;
        particle.transform.parent = targetEnemy;
        StartCoroutine(PlayParticle(particle));
    }

    IEnumerator PlayParticle(GameObject particle) {
        particle.GetComponent<ParticleSystem>().Play();
        yield return new WaitForSeconds(0.2f);
        Destroy(particle);
    }
}
