﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Unit : MonoBehaviour {

    [Header("Unit Attributes")]
    public int health;
    public int damage;
    public int speed;
    public float attackRate;
    public bool canAttack = true;
    NavMeshAgent navMeshAgent;

    private void Awake() {
        navMeshAgent = GetComponent<NavMeshAgent>();
        navMeshAgent.speed = speed;
    }

    public void SetDestinationTo(Transform destination) {
        navMeshAgent.SetDestination(destination.position);
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "AllyFinish") {
            GameManager.Instance.GainHealhtPoints();
            Destroy(gameObject);
        }
        if (other.tag == "Enemy") {
            Attack(other.GetComponent<Enemy>());
        }
    }

    //Atacar
    public void Attack(Enemy enemy) {
        if (canAttack) {
            enemy.ReceiveDamage(damage);
            StartCoroutine(AttackCooldown());
        }        
    }
    IEnumerator AttackCooldown() {
        navMeshAgent.isStopped = true;
        canAttack = false;
        yield return new WaitForSeconds(attackRate);
        navMeshAgent.isStopped = false;
        canAttack = true;
    }
    //Daño directo
    public void ReceiveDamage(int receivedDamage) {
        health -= receivedDamage;
        if (health <= 0) {
            GameManager.Instance.currentPopulation--;
            GameManager.Instance.UpdateResourcesUI();
            Destroy(gameObject);
        }
    }
    public void ReceiveDamage(float receivedDamage) {
        health -= Mathf.RoundToInt(receivedDamage);
        if (health <= 0) {            
             GameManager.Instance.currentPopulation--;
            GameManager.Instance.UpdateResourcesUI();
            Destroy(gameObject);
        }
    }   
}
