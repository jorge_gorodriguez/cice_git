﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitManager : MonoBehaviour {

    [Header("Prefabs")]
    public GameObject workerPrefab;
    public GameObject warriorPrefab;
    public GameObject defenderPrefab;
    public GameObject gymOnePrefab;
    [Header("Locations")]    
    public Transform allySpawn;
    public Transform allyFinish;

    //Singleton
    private static UnitManager _instance;
    [HideInInspector] public static UnitManager Instance { get { return _instance; } }
    private void Awake() { if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; } }

    //Diccionario con los tipos de unidades y su coste asociado
    public Dictionary<UnitTypes, UnitCost> unitsCostAtlas = new Dictionary<UnitTypes, UnitCost>();

    public void InitializeUnitsPrices() {
        unitsCostAtlas = new Dictionary<UnitTypes, UnitCost>();
        unitsCostAtlas.Add(UnitTypes.WORKER, new UnitCost(20, 20, 50, 50, 1));
        unitsCostAtlas.Add(UnitTypes.WARRIOR, new UnitCost(30, 30, 20, 20, 2));
        unitsCostAtlas.Add(UnitTypes.DEFENDER, new UnitCost(40, 40, 50, 10, 2));
        unitsCostAtlas.Add(UnitTypes.GYM_ONE, new UnitCost(100, 50, 10, 50, 2));
    }

    //Llamada desde UI Action Manager al pulsar el botón correspondiente
    public void CreateNewUnit(UnitTypes unitType) {
        GameObject newUnit = null;
        switch (unitType) {
            case UnitTypes.WORKER:
                newUnit = Instantiate<GameObject>(workerPrefab, allySpawn);
                GameManager.Instance.workerManager.RegisterNewWorker(newUnit.GetComponent<Worker>());
                break;
            case UnitTypes.WARRIOR:
                newUnit = Instantiate<GameObject>(warriorPrefab, allySpawn);
                break;
            case UnitTypes.DEFENDER:
                newUnit = Instantiate<GameObject>(defenderPrefab, allySpawn);
                break;
            case UnitTypes.GYM_ONE:
                newUnit = Instantiate<GameObject>(gymOnePrefab, allySpawn);
                break;
        }
        newUnit.transform.localPosition = Vector3.zero;
        GameManager.Instance.PayUnit(unitsCostAtlas[unitType]);
        if (unitType != UnitTypes.WORKER) {
            RegisterNewUnit(newUnit.GetComponent<Unit>());
        }
    }

    public void RegisterNewUnit(Unit unit) {
        unit.gameObject.SetActive(true);
        unit.SetDestinationTo(allyFinish);
    }

}
