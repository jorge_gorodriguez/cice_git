﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Score {

    [SerializeField] public int gold;
    [SerializeField] public int wood;
    [SerializeField] public int food;
    [SerializeField] public int water;
    [SerializeField] public int health;
    [SerializeField] public int unemployeds;
    [SerializeField] public int builders;
    [SerializeField] public int miners;
    [SerializeField] public int woodcutters;
    [SerializeField] public int farmers;
    [SerializeField] public int waterMasters;    

    public Score(int gold, int wood, int food, int water, int health, int unemployeds, int builders, int miners, int woodcutters, int farmers, int waterMasters) {
        this.gold = gold;
        this.wood = wood;
        this.food = food;
        this.water = water;
        this.health = health;
        this.unemployeds = unemployeds;
        this.builders = builders;
        this.miners = miners;
        this.woodcutters = woodcutters;
        this.farmers = farmers;
        this.waterMasters = waterMasters;
    }

    public void StoreJson(string key, string value) {
        PlayerPrefs.SetString(key, value);
    }
}
