﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public Button startGameButton;
    public Button helpButton;
    public Button quitButton;
    public Button backButton;
    public GameObject helpPanel;

    private void Start() {
        startGameButton.onClick.AddListener(delegate { StartNewGame(); });
        quitButton.onClick.AddListener(delegate { QuitGame(); });
        helpButton.onClick.AddListener(delegate { ShowHelpPanel(); });
        backButton.onClick.AddListener(delegate { HideHelpPanel(); });
    }

    public void StartNewGame() {
        SceneManager.LoadScene("main");
    }
    public void ShowHelpPanel() {
        helpPanel.SetActive(true);
    }
    public void HideHelpPanel() {
        helpPanel.SetActive(false);
    }
    public void QuitGame() {
        Application.Quit();
    }
}
