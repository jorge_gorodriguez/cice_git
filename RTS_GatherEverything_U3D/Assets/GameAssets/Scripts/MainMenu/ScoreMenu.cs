﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScoreMenu : MonoBehaviour {
    [Header("Buttons")]
    public Button playAgainButton;
    public Button quitButton;

    [Header("Score")]
    public Text goldText;
    public Text woodText;
    public Text foodText;
    public Text waterText;
    public Text healthText;
    public Text unemployedsText;
    public Text buildersText;
    public Text minersText;
    public Text woodcuttersText;
    public Text farmersText;
    public Text waterMastersText;


    private void Start() {
        playAgainButton.onClick.AddListener(delegate { StartNewGame(); });
        quitButton.onClick.AddListener(delegate { QuitGame(); });     
        
    }

    public void StartNewGame() {
        SceneManager.LoadScene("main");
    }
    
    public void LoadScoreInfo() {
        Score lastScore = JsonUtility.FromJson<Score>(PlayerPrefs.GetString("lastScore"));
        goldText.text = lastScore.gold.ToString("D2");
        woodText.text = lastScore.wood.ToString("D2");
        foodText.text = lastScore.food.ToString("D2");
        waterText.text = lastScore.water.ToString("D2");
        healthText.text = lastScore.health.ToString("D2");
        unemployedsText.text = lastScore.unemployeds.ToString("D2");
        buildersText.text = lastScore.builders.ToString("D2");
        minersText.text = lastScore.miners.ToString("D2");
        woodcuttersText.text = lastScore.woodcutters.ToString("D2");
        farmersText.text = lastScore.farmers.ToString("D2");
        waterMastersText.text = lastScore.waterMasters.ToString("D2");
        PlayerPrefs.DeleteAll();
    }

    public void QuitGame() {
        Application.Quit();
    }
}
