﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitCost: CostBase {

    public UnitCost() {
        GoldCost = 0;
        WoodCost = 0;
        FoodCost = 0;
        WaterCost = 0;
        PopulationCost = 0;
    }

    public UnitCost(int goldCost, int woodCost, int foodCost, int waterCost, int populationCost) {
        GoldCost = goldCost;
        WoodCost = woodCost;
        FoodCost = foodCost;
        WaterCost = waterCost;
        PopulationCost = populationCost;
    }
}
