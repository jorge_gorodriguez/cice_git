﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCost : CostBase {

    public BuildingCost() {
        GoldCost = 0;
        WoodCost = 0;
        FoodCost = 0;
        WaterCost = 0;
    }

    public BuildingCost(int goldCost, int woodCost, int foodCost, int waterCost) {
        GoldCost = goldCost;
        WoodCost = woodCost;
        FoodCost = foodCost;
        WaterCost = waterCost;
    }
}
