﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CostBase {

    int goldCost;
    public int GoldCost { get { return goldCost; } set { goldCost = value; } }
    int woodCost;
    public int WoodCost { get { return woodCost; } set { woodCost = value; } }
    int foodCost;
    public int FoodCost { get { return foodCost; } set { foodCost = value; } }
    int waterCost;
    public int WaterCost { get { return waterCost; } set { waterCost = value; } }
    int populationCost;
    public int PopulationCost { get { return populationCost; } set { populationCost = value; } }


    

}
