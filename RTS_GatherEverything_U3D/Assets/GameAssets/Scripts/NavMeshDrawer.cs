﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshDrawer : MonoBehaviour {

    private void Start() {
        NavMeshTriangulation navMeshTriangulation = NavMesh.CalculateTriangulation();
        Debug.Log(navMeshTriangulation);
        foreach (Vector3 vertice in navMeshTriangulation.vertices) {
            GameObject newVertex = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            newVertex.GetComponent<Renderer>().material.color = Color.red;
            newVertex.transform.position = vertice;
        }
        
    }

}
