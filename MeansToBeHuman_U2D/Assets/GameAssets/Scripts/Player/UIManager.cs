﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public GameObject deadUI;

    public Text healthBarText;
    public Image healthBarFGImage;
    public Text experiencePointsText;
    public Text deadExperiencePointsText;
    public Text coinsText;
    public Text deadCoinsText;

    //Singleton
    private static UIManager _instance;
    [HideInInspector] public static UIManager Instance { get { return _instance; } }
    private void Awake() {
        if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; }
    }

    public void PlayAgain() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitGame() {
        Application.Quit();
    }

    public  void HandleDead() {
        deadUI.SetActive(true);
        deadCoinsText.text = coinsText.text;
        deadExperiencePointsText.text = experiencePointsText.text;
    }

    public void HandleHealthBar(float currentHealth, float maxHealth) {
        healthBarText.text = currentHealth.ToString();
        float newFillAmount = (currentHealth / maxHealth);
        healthBarFGImage.fillAmount = newFillAmount;
    }
    public void HandleExperiencePoints(int experiencePoints) {
        experiencePointsText.text = experiencePoints.ToString();
    }
    public void HandleCoins(int coins) {
        coinsText.text = coins.ToString();
    }

    public void HandleKeys(int normalKeys, int bossKeys) {
        experiencePointsText.text = normalKeys.ToString();
        coinsText.text = bossKeys.ToString();
    }

    IEnumerator TextForSeconds(Text uiText, string text, float time) {
        uiText.text = text;
        yield return new WaitForSeconds(time);
        uiText.text = "";
    }    
}
