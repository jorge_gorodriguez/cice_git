﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour {

    public float movementSpeed = 25;

    Character2DController controller;
    Animator animator;

    float horizontalMovement;
    public float HorizontalMovement { get { return horizontalMovement * Time.fixedDeltaTime * movementSpeed; } }
    [HideInInspector] public bool canJump = false;
    [HideInInspector] public bool canDoublejump = false;
    [HideInInspector] public bool isGrounded = false;
    [HideInInspector] public bool isCrouching = false;

    void Start() {
        controller = GetComponent<Character2DController>();
        animator = GetComponent<Animator>();
    }

    void Update() {
        horizontalMovement = Input.GetAxisRaw("Horizontal");
        animator.SetFloat("Speed", Mathf.Abs(horizontalMovement));

        if (Input.GetKeyDown(KeyCode.Space)) {            
            if (isGrounded) {
                canDoublejump = true;
                isGrounded = false;
                Jump(true);
            } else {
                if (canDoublejump) {
                    canDoublejump = false;
                    Jump(true);
                }
            }
        }

        if (isGrounded == true) {
            if (Input.GetKeyDown(KeyCode.LeftControl)) {
                Crouch(true);                
            }
            if (Input.GetKeyUp(KeyCode.LeftControl)) {
                isCrouching = false;
                if (controller.m_wasCrouching == false) {
                    Crouch(false);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.X)) {
            animator.SetTrigger("Attack");
        }
    }
    void FixedUpdate() {
        controller.Move(horizontalMovement * movementSpeed * Time.fixedDeltaTime, isCrouching, canJump);
        canJump = false;
    }

    public void Jump(bool value) {
        canJump = value;
        animator.SetBool("IsJumping", value);
    }
    public void DoubleJump(bool value) {
        canDoublejump = value;
        animator.SetBool("IsJumping", value);
    }
    public void Crouch(bool value) {
        isCrouching = value;
        animator.SetBool("IsCrouching", value);
    }

    public void HitGround() {
        animator.SetBool("IsJumping", false);
        //isGrounded = true;
    }
    public void OnCrouch(bool crouch) {
        animator.SetBool("IsCrouching", crouch);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.transform.tag == "Ground") {
            isGrounded = true;
            canDoublejump = true;
        }
	}
    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.transform.tag == "Ground") {
            isGrounded = false;
        }
    }
}
