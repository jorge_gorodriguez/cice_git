﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    //Atributos del player. Las variables son privadas, para no modificarlas donde no se debe, pero son mostradas en el inspector para darles unos valores iniciales
    [SerializeField] private int currentExperiencePoints;
    public int ExpPoints { get { return currentExperiencePoints; } set { currentExperiencePoints = value; } }
    [SerializeField] private int currentCoins;
    public int Coins { get { return currentCoins; } set { currentCoins = value; } }
    [SerializeField] private int maxHealth;
    public int MaxHealth { get { return maxHealth; }  set { maxHealth = value; } }
    [SerializeField] private int currentHealth;
    public int CurrentHealth { get { return currentHealth; } set { currentHealth = value; } }    
    public int damage;
    public bool canReceiveDamage = true;
    Animator animator;
    Character2DController controller;

    //Singleton
    private static PlayerManager _instance;
    [HideInInspector] public static PlayerManager Instance { get { return _instance; } }
    private void Awake() {
        if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; }
    }

    private void Start() {
        controller = GetComponent<Character2DController>();
        HealDamage(maxHealth);
        currentExperiencePoints = 0;
        currentCoins = 0;
        animator = GetComponent<Animator>();
    }
    private void Update() {
        if (currentHealth<0) {
            UIManager.Instance.HandleDead();
        }
    }    

    void FixedUpdate() {
        //Lógica para golpear a algunos enemigos (esqueletos) saltando encima suya
        RaycastHit2D hit = Physics2D.Raycast(transform.Find("GroundCheck").position, -Vector2.up, 10);
        Debug.DrawRay(transform.Find("GroundCheck").position, -Vector2.up, Color.red);
        if (hit.collider != null) {
            if (hit.transform.tag == "Enemy") {
                if (hit.distance <= 0.1) {
                    if (hit.transform.GetComponent<EnemyBase>().canBeSmashed) {
                        hit.transform.GetComponent<EnemyBase>().GetHurt();
                    }
                }                
            }
        }
    }
    //Funciones para cambiar los atributos del Player
    public void ChangeExperience(int experiencePoints) {
        currentExperiencePoints += experiencePoints;
        UIManager.Instance.HandleExperiencePoints(currentExperiencePoints);
    }
    public void ChangeCoins(int coins) {
        currentCoins += coins;
        UIManager.Instance.HandleCoins(currentCoins);
    }
    public void HealDamage(int heal) {
        currentHealth += heal;
        UIManager.Instance.HandleHealthBar(currentHealth, maxHealth);
    }
    public void ReceiveDamage(int damage) {
        if (canReceiveDamage) {
            GetHurt(2f);
            currentHealth -= damage;
            Immunize(2f);
        }
        UIManager.Instance.HandleHealthBar(currentHealth, maxHealth);
    }
    public void GetHurt(float duration) {
        StartCoroutine(Hurt(duration));
    }
    IEnumerator Hurt(float respawnTime) {
        animator.SetBool("isHurt", true);
        yield return new WaitForSeconds(respawnTime);
        animator.SetBool("isHurt", false);
    }
    //Lógica para volverse inmune durante un tiempo
    public void Immunize(float immuneDuration) {
        StartCoroutine(ImmuneOverTime(immuneDuration));
    }
    //Con esta corrutina establecemos la ventana en la que el player es inmune, m
    IEnumerator ImmuneOverTime(float immuneDuration) {
        canReceiveDamage = false;
        float horizontalMovement = GetComponent<PlayerInputController>().HorizontalMovement;
        controller.Move(-horizontalMovement, false, true);
        StartCoroutine(FadeTo(GetComponent<SpriteRenderer>(), 0, immuneDuration / 4));
        yield return new WaitForSeconds(immuneDuration/2);
        StartCoroutine(FadeTo(GetComponent<SpriteRenderer>(), 1, immuneDuration / 4));
        canReceiveDamage = true;
    }
    IEnumerator FadeTo(SpriteRenderer spriteRenderer, float targetOpacity, float duration) {
        float elapsedTime = 0;
        Color color = spriteRenderer.color;
        float startOpacity = color.a;
        while (elapsedTime < duration) {
            elapsedTime += Time.deltaTime;
            float blend = Mathf.Clamp01(elapsedTime / duration);
            color.a = Mathf.Lerp(startOpacity, targetOpacity, blend);
            spriteRenderer.color = color;
            yield return null;
        }
    }

}
