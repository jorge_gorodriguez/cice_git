﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public static class ExtensionMethods {

    public static string GetFullPath(this Transform current) {
        if (current.parent == null)
            return "/" + current.name;
        return current.parent.GetFullPath() + "/" + current.name;
    }
    public static void Resize<T>(this List<T> list, int newCount) {
        if (newCount <= 0) {
            list.Clear();
        } else {
            while (list.Count > newCount) list.RemoveAt(list.Count - 1);
            while (list.Count < newCount) list.Add(default(T));
        }
    }
    public static string DeleteLastChar(this string current) {
        List<char> characters = new List<char>(current.ToCharArray());
        int size = characters.Count;
        characters.RemoveAt(size - 1);
        current = new string(characters.ToArray());
        return current;
    }
}