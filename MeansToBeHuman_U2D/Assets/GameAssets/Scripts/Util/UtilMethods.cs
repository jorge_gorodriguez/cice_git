﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UtilMethods : MonoBehaviour {

    //Slow
    public static GameObject FindInChildrenIncludingInactive(GameObject gameObject, string name) {
        for (int i = 0; i < gameObject.transform.childCount; i++) {
            if (gameObject.transform.GetChild(i).transform.GetFullPath() == name) return gameObject.transform.GetChild(i).gameObject;
            GameObject found = FindInChildrenIncludingInactive(gameObject.transform.GetChild(i).gameObject, name);
            if (found != null) return found;
        }
        return null;  //couldn't find
    }

    //Slow
    public static GameObject FindIncludingInactive(string gameObjectPath) {
        Scene scene = SceneManager.GetActiveScene();
        List<GameObject> game_objects = new List<GameObject>();
        bool isRootObject = false;
        string pathSeparator = "/";
        int stepsCount = (gameObjectPath.Length - gameObjectPath.Replace(pathSeparator, "").Length) / pathSeparator.Length;
        if (stepsCount == 1) {
            isRootObject = true;
        }
        scene.GetRootGameObjects(game_objects);
        foreach (GameObject obj in game_objects) {
            GameObject found = null;
            if (isRootObject) {
                if (obj.activeSelf) {
                    if (obj.transform.GetFullPath() == gameObjectPath) {
                        return obj;
                    }
                } else {
                    string gameObjectName = gameObjectPath.Split('/')[1];
                    if (obj.name == gameObjectName) {
                        return obj;
                    }
                }
            } else {
                found = FindInChildrenIncludingInactive(obj, gameObjectPath);
            } 
            if (found) return found;
        }
        return null;
    }
}
