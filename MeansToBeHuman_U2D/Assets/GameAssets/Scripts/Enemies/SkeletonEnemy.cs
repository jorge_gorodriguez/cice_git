﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SkeletonEnemy : EnemyBase {

    public float respawnTime = 2;

    public void Start() {        
        enemyName = CleanEnemyName();
        //Ignore Collision between 10-Player and 11-Enemy
        Physics2D.IgnoreLayerCollision(10, 11); 
        Physics2D.IgnoreLayerCollision(11, 11);
        animator = this.GetComponent<Animator>();
        animator.SetTrigger("Rise");
    }

    
    // Update is called once per frame
    void FixedUpdate () {
        //En todos los enemigos utilizamos de una manera u otra el Character2DController para el movimiento
        if (animator.GetCurrentAnimatorStateInfo(0).IsName(enemyName + "_Walking")) { 
            gameObject.GetComponent<Character2DController>().Move(horizontalMovement * movementSpeed * Time.fixedDeltaTime, false, false);
        }       
	}

    //El esqueleto nunca muere, cada vez que se le hiere permanece un rato haciendose el muerto y luego vuelve
    public override void GetHurt() {
        PlayerManager.Instance.ChangeExperience(1);
        StartCoroutine(Die(respawnTime));
    }

    IEnumerator Die(float respawnTime) {
        animator.SetBool("isDying", true);
        yield return new WaitForSeconds(respawnTime);
        animator.SetBool("isDying", false);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Sword") {
            GetHurt();
        }
        if (collision.transform.tag == "EnemyBlocker") { //Sirve para darle la vuelta cuando choque con los enemyBlockers que hay por las pantallas
            horizontalMovement *= -1;
        }
    }
}
