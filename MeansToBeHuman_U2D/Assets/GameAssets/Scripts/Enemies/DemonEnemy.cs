﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

public class DemonEnemy : EnemyBase {

    public float hurtCooldown = 2;
    public float detectionRadius = 2.5f;
    public float hitDistance = 1.5f;
    public float detectionOffset = 0.5f;
    public bool isAlreadyPatrolling = false;
    bool isDead = false;
    
    public void Start() {
        CurrentHealth = MaxHealth;
        transform.Find("Enemy_DemonProjectile").gameObject.GetComponent<EnemyProjectile>().damage = damage;
        enemyName = CleanEnemyName();
        //Ignorar las colisiones entre los layers 10-Player and 11-Enemy
        Physics2D.IgnoreLayerCollision(10, 11);
        Physics2D.IgnoreLayerCollision(11, 11);
        animator = this.GetComponent<Animator>();
    }

    void FixedUpdate() {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName(enemyName + "_Idle")) {
            gameObject.GetComponent<Character2DController>().Move(horizontalMovement * movementSpeed * Time.fixedDeltaTime, false, false, true, verticalMovement);
        }
        //Casteamos un circulo alrededor del enemigo
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, detectionRadius + detectionOffset, Vector2.down);
        //si detecta algo y es el Player...
        if (hit.collider != null) {
            if (hit.transform.tag == "Player") {
                Vector2 enemyPosition = new Vector2(transform.position.x, transform.position.y);
                float distanceToEnemy = Mathf.Abs((hit.point - enemyPosition).magnitude);                
                if (distanceToEnemy <= hitDistance) { //Si estamos a distancia de tiro, atacamos                    
                    if (canAttack) {
                        StopChasing();
                        StartAttack();
                    }
                } else if (distanceToEnemy <= detectionRadius) { //Si estamos a distancia de persecución, perseguimos
                    if (isAlreadyPatrolling) {
                        isAlreadyPatrolling = false;
                        StopRandomPatrol();
                    }
                    StartChasing();
                }
            } else {
                StopChasing();
                if (!isAlreadyPatrolling) {
                    isAlreadyPatrolling = true;
                    StartRandomPatrol();
                }
            }
        } else {
            //Si no detectamos nada, que casi nunca va a suceder
        }
    }        

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Sword") {
            GetHurt();
        }
        if (collision.transform.tag == "EnemyBlocker") {
            horizontalMovement *= -1;
        }
    }

    //Lógica sobre morir
    public override void IsDying() {
        StartCoroutine(Die(hurtCooldown));
    }
    IEnumerator Die(float waitTime) {
        animator.SetTrigger("Die");
        yield return new WaitForSeconds(2);
        yield return new WaitForEndOfFrame();
        Destroy(this.gameObject);
    }

    //Lógica sobre ser herido
    public override void GetHurt() {
        ReceiveDamage(PlayerManager.Instance.damage);
        if (CurrentHealth < 0) {
            if (!isDead) PlayerManager.Instance.ChangeExperience(100);
            IsDying();
        } else {
            StartCoroutine(Hurt(hurtCooldown));
        }
    }
    IEnumerator Hurt(float respawnTime) {
        //Disparamos la animación de ser herido, bloqueamos la capacidad de atacar y después de un tiempo, volvemos al estado inicial
        animator.SetBool("isHurt", true);
        canAttack = false;
        yield return new WaitForSeconds(respawnTime);
        animator.SetBool("isHurt", false);
        canAttack = true;
    }

    //Lógica sobre atacar
    public override void StartAttack() {
        StartCoroutine(Attack());
    }
    IEnumerator Attack() {
        animator.SetTrigger("Attack");
        canAttack = false;
        //Con esto reorientamos al enemigo antes de atacar
        Vector2 distance = PlayerManager.Instance.transform.position - transform.position;
        if (distance.normalized.x < 0) { //Si el Player está a la izquierda
            horizontalMovement = horizontalMovement < 0 ? horizontalMovement : horizontalMovement * -1;
        } else if (distance.normalized.x > 0) { //Y si está a la derecha
            horizontalMovement = horizontalMovement > 0 ? horizontalMovement : horizontalMovement * -1;
        }
        yield return new WaitForSeconds(attackCooldown);
        canAttack = true;
    }

    //Lógica sobre perseguir al Player
    public void StartChasing() {
        StartCoroutine(Chasing());
    }
    public void StopChasing() {
        StopCoroutine(Chasing());
    }
    IEnumerator Chasing() {
        while (true) {
            yield return new WaitForSeconds(5);
            //Calculamos la distancia desde el Enemigo actual al Player
            Vector2 distance = PlayerManager.Instance.transform.position - transform.position;
            horizontalMovement = distance.normalized.x;
            verticalMovement = distance.normalized.y / 10;
        }
    }

    //Lógica sobre patrullar aleatoriamente
    public void StartRandomPatrol() {
        StartCoroutine(RandomPatrol());
    }
    public void StopRandomPatrol() {
        StopCoroutine(RandomPatrol());
    }
    IEnumerator RandomPatrol() {
        while (true) {
            //Calculamos aleatoriamente los valores Vertical y Horizontal Movement
            yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
            verticalMovement = Random.Range(-0.05f, 0.05f);
            yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
            horizontalMovement = Random.Range(-2f, 2f);
        }
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(transform.position, hitDistance);
        Gizmos.DrawWireSphere(transform.position, detectionRadius);
        Gizmos.DrawWireSphere(transform.position, detectionRadius + detectionOffset);
    }
}
