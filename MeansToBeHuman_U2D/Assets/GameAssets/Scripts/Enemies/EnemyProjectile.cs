﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour {

    public int damage;

    private void FixedUpdate() {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, 1, Vector2.down);
        if (hit.collider != null) {
            if (hit.transform.tag == "Player") {
                PlayerManager.Instance.ReceiveDamage(damage);
            }
        }
    }
}
