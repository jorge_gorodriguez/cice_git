﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class HellHoundEnemy : EnemyBase {

    public float hurtCooldown = 2;
    public float chaseSpeed = 5;
    public float detectionRadius;
    public float hitDistance;
    public float detectionOffset;
    public bool isAlreadyPatrolling = false;
    public bool isAlreadyChasing = false;
    bool isDead = false;

    public void Start() {
        CurrentHealth = MaxHealth;
        transform.Find("Enemy_HellHound").Find("Enemy_HellHoundMeleeAttack").gameObject.GetComponent<EnemyMeleeAttack>().damage = damage;
        enemyName = CleanEnemyName();
        //Ignorar las colisiones entre los layers 10-Player and 11-Enemy
        Physics2D.IgnoreLayerCollision(10, 11);
        Physics2D.IgnoreLayerCollision(11, 11);
        animator = this.GetComponent<Animator>();
        StartRandomPatrol();
    }
   
    void FixedUpdate() {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName(enemyName + "_Idle") || animator.GetCurrentAnimatorStateInfo(0).IsName(enemyName + "_Walk") || animator.GetCurrentAnimatorStateInfo(0).IsName(enemyName + "_Run")) {
            if (isAlreadyChasing) {
                gameObject.GetComponent<Character2DController>().Move(horizontalMovement * chaseSpeed * Time.fixedDeltaTime, false, false, true);
            } else {
                gameObject.GetComponent<Character2DController>().Move(horizontalMovement * movementSpeed * Time.fixedDeltaTime, false, false, true);
            }
        }
        //Casteamos un circulo alrededor del enemigo
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, detectionRadius + detectionOffset, Vector2.right);
        //si detecta algo y es el Player...
        if (hit.collider != null) {
            if (hit.transform.tag == "Player") {
                Vector2 enemyPosition = new Vector2(transform.position.x, transform.position.y);
                float distanceToEnemy = Mathf.Abs((hit.point - enemyPosition).magnitude);
                if (distanceToEnemy <= hitDistance) { //Si estamos a distancia de tiro, atacamos                    
                    if (canAttack) {
                        isAlreadyChasing = false;
                        StopChasing();
                        StartAttack();
                    }
                } else if (distanceToEnemy <= detectionRadius) { //Si estamos a distancia de persecución, perseguimos
                    if (isAlreadyPatrolling) {
                        isAlreadyPatrolling = false;
                        StopRandomPatrol();
                    }
                    isAlreadyChasing = true;
                    StartChasing();
                }
            } else {
                StopChasing();
                if (!isAlreadyPatrolling) {
                    isAlreadyPatrolling = true;
                    isAlreadyChasing = false;
                    StartRandomPatrol();
                }
            }
        } else {
            //Si no detectamos nada, que casi nunca va a suceder
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Sword") {
            GetHurt();
        }
        if (collision.transform.tag == "EnemyBlocker") {
            horizontalMovement *= -1;
        }
    }

    //Lógica sobre morir
    public override void IsDying() {
        StartCoroutine(Die(hurtCooldown));
    }
    IEnumerator Die(float waitTime) {
        animator.SetTrigger("Die");
        yield return new WaitForSeconds(2);
        yield return new WaitForEndOfFrame();
        Destroy(this.gameObject);
    }

    //Lógica sobre ser herido
    public override void GetHurt() {
        ReceiveDamage(PlayerManager.Instance.damage);
        if (CurrentHealth < 0) {
            if (!isDead) PlayerManager.Instance.ChangeExperience(100);
            IsDying();
        } else {
            StartCoroutine(Hurt(hurtCooldown));
        }
    }
    IEnumerator Hurt(float respawnTime) {
        animator.SetBool("isHurt", true);
        canAttack = false;
        yield return new WaitForSeconds(respawnTime);
        animator.SetBool("isHurt", false);
        canAttack = true;
    }

    //Lógica sobre atacar
    public override void StartAttack() {
        StartCoroutine(Attack());
    }
    IEnumerator Attack() {
        animator.SetTrigger("Attack");
        canAttack = false;
        //Con esto reorientamos al enemigo antes de atacar
        Vector2 distance = PlayerManager.Instance.transform.position - transform.position;
        if (distance.normalized.x < 0) { //Si el Player está a la izquierda
            horizontalMovement = horizontalMovement < 0 ? horizontalMovement : horizontalMovement * -1;
        } else if (distance.normalized.x > 0) { //Y si está a la derecha
            horizontalMovement = horizontalMovement > 0 ? horizontalMovement : horizontalMovement * -1;
        }
        yield return new WaitForSeconds(attackCooldown);
        canAttack = true;
    }

    //Lógica sobre perseguir al Player
    public void StartChasing() {
        StartCoroutine(Chasing());
    }
    public void StopChasing() {
        StopCoroutine(Chasing());
    }
    IEnumerator Chasing() {
        while (true) {
            yield return new WaitForSeconds(5);
            animator.SetFloat("Speed", 0.04f);
            Vector2 distance = PlayerManager.Instance.transform.position - transform.position;
            horizontalMovement = distance.normalized.x;
        }
    }

    //Lógica sobre patrullar aleatoriamente
    public void StartRandomPatrol() {
        StartCoroutine(RandomPatrol());
    }
    public void StopRandomPatrol() {
        StopCoroutine(RandomPatrol());
    }
    IEnumerator RandomPatrol() {
        while (true) {
            yield return new WaitForSeconds(Random.Range(1f, 3f));
            float animatorSpeed = Random.Range(0, 5) == 0 ? 0f : 0.02f;
            animator.SetFloat("Speed", animatorSpeed);
            if (animatorSpeed == 0) {
                horizontalMovement = 0;
            } else {
                horizontalMovement = Random.Range(-2f, 2f);
            }
        }
    }

    private void OnDrawGizmos() {
        Debug.DrawRay(transform.position - new Vector3(0f, 0.02f, 0f), Vector2.left * (detectionRadius + detectionOffset), Color.blue);
        Debug.DrawRay(transform.position, Vector2.left * detectionRadius, Color.yellow);
        Debug.DrawRay(transform.position + new Vector3(0f, 0.02f, 0f), Vector2.left * hitDistance, Color.red);
        Debug.DrawRay(transform.position - new Vector3(0f, 0.02f, 0f), Vector2.right * (detectionRadius + detectionOffset), Color.blue);
        Debug.DrawRay(transform.position, Vector2.right * detectionRadius, Color.yellow);
        Debug.DrawRay(transform.position + new Vector3(0f, 0.02f, 0f), Vector2.right * hitDistance, Color.red);
    }
}
