﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;


//Clase base de los enemigos, contiene la mayor parte de sus atributos y algunas funciones muy comunes
public class EnemyBase : MonoBehaviour {
    public int damage;
    [SerializeField] private int maxHealth;
    public int MaxHealth { get { return maxHealth; } set { maxHealth = value; } }
    [SerializeField] private int currentHealth;
    public int CurrentHealth { get { return currentHealth; } set { currentHealth = value; } }    
    [HideInInspector] public string enemyName;
    public float movementSpeed = 1;
    [HideInInspector] public float horizontalMovement = -1;
    [HideInInspector] public float verticalMovement = 1;
    public bool canBeSmashed = false;
    [HideInInspector] public bool canAttack = true;
    public float attackCooldown = 2f;
    [HideInInspector] public Animator animator;



    //Método virtual para activar las animaciones, efectos y consecuencias de ser herido. Implementado en los hijos si las necesito
    public virtual void StartAttack() { }
    public virtual void GetHurt() { }
    public virtual void IsDying() { }

    public void ReceiveDamage(int damage) {
        currentHealth -= damage;
    }

    public string CleanEnemyName() {
        //Expresión regular para capturar patrones de texto tipo: "(" + "número" + ")", es decir, un número entre parentesis, como (1)
        string pattern = @"\(\d+\).*?";
        //Usando la expresión regular eliminamos los parentesis del nombre para no hardcodear las animaciones y lo devolvemos al hijo que lo reclame
        string input = gameObject.name.Split('_')[1];
        Match match = Regex.Match(input, pattern, RegexOptions.IgnoreCase);
        string cleanEnemyName = string.IsNullOrEmpty(match.Value) ? input : input.Replace(match.Value, "");
        return cleanEnemyName;
    }
}
