﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject followedGO;
    public float xOffset;	
    public float yOffset;
    public float zoomSpeed;
    PlayerInputController playerInputController;

    private void Start() {
        playerInputController = followedGO.GetComponent<PlayerInputController>();
    }

    // Update is called once per frame
    void Update () {
        this.transform.position = new Vector3(followedGO.transform.position.x + xOffset, followedGO.transform.position.y + yOffset, this.transform.position.z);
        if (!playerInputController.isGrounded) {
            if (Camera.main.fieldOfView > 45) {
                Camera.main.fieldOfView -= zoomSpeed * Time.deltaTime;
            }
        } else {
            if (Camera.main.fieldOfView < 60) {
                Camera.main.fieldOfView += zoomSpeed * Time.deltaTime;
            }
        }
    }
}
