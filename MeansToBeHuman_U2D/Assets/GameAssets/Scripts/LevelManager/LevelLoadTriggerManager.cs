﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoadTriggerManager : MonoBehaviour {

    public LevelTransitions levelTransition;

    public void AssignTriggers() {
        levelTransition.AToBTrigger.gameObject.AddComponent<LevelLoadTrigger>();
        levelTransition.AToBTrigger.gameObject.GetComponent<LevelLoadTrigger>().levelToLoad = levelTransition.bLevel;
        levelTransition.AToBTrigger.gameObject.GetComponent<LevelLoadTrigger>().entrancePosition = levelTransition.BFromAEntrance;
        levelTransition.BToATrigger.gameObject.AddComponent<LevelLoadTrigger>();
        levelTransition.BToATrigger.gameObject.GetComponent<LevelLoadTrigger>().levelToLoad = levelTransition.aLevel;
        levelTransition.BToATrigger.gameObject.GetComponent<LevelLoadTrigger>().entrancePosition = levelTransition.AFromBEntrance;
    }
}
