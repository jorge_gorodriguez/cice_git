﻿using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum EntrancePosition {
    LEFT, TOP, RIGHT, BOTTOM
}

[Serializable]
public class LevelTransitions {
    public string hallwayName;
    public string aLevel;
    public string bLevel;
    public BoxCollider2D AToBTrigger;
    public BoxCollider2D BToATrigger;
    public EntrancePosition AFromBEntrance;
    public EntrancePosition BFromAEntrance;
}

public class LevelManager : MonoBehaviour {

    public GameObject hallways;
    public List<LevelTransitions> levelTransitions;
    [HideInInspector] public Vector2 spawnPosition;
    [HideInInspector] public GameObject virtualCamera;
    [HideInInspector] public GameObject cameraConfiner;

    //Singleton
    private static LevelManager _instance;
    [HideInInspector] public static LevelManager Instance { get { return _instance; } }
    private void Awake() {
        if (_instance != null && _instance != this) { Destroy(this.gameObject); } else { _instance = this; }
    }

    //Eventos de carga de nivel. 
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoadedRefill;
    }
    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoadedRefill;
    } 



    void OnSceneLoadedRefill(Scene scene, LoadSceneMode mode) {
        Debug.Log("OnSceneLoadedRefill");
        levelTransitions = new List<LevelTransitions>();
        levelTransitions = FillLevelTransitionsList();
    }
    void OnSceneLoadedSpawnPlayerAndCamera(Scene scene, LoadSceneMode mode) {
        Debug.Log("OnSceneLoadedSpawnPlayerAndCamera");        
        PlayerManager.Instance.transform.position = new Vector2(PlayerPrefs.GetFloat("spawnPositionX"), PlayerPrefs.GetFloat("spawnPositionY"));
        GameObject virtualCamera = GameObject.FindGameObjectWithTag("VirtualCamera");
        virtualCamera.GetComponent<CinemachineConfiner>().m_BoundingShape2D = GameObject.Find(PlayerPrefs.GetString("pathToConfiner")).GetComponent<Collider2D>();
    }    
    void OnSceneLoadedResetPlayerPrefs(Scene scene, LoadSceneMode mode) {
        Debug.Log("OnSceneLoadedResetPlayerPrefs");        
        PlayerPrefs.DeleteAll();
    }

    //Recorremos la jerarquía de los pasillos, repartiendo en función del os trigger de entrada y salida de los mismos, el comportamiento esperado en estos
    public List<LevelTransitions> FillLevelTransitionsList() {
        List<LevelTransitions> levelTransitions = new List<LevelTransitions>();
        for (int i = 0; i < hallways.transform.childCount; i++) {
            GameObject hallway = hallways.transform.GetChild(i).gameObject;
            LevelLoadTriggerManager newLevelLoadTriggerManager = hallway.AddComponent<LevelLoadTriggerManager>();
            LevelTransitions auxLevelTransition = new LevelTransitions();
            auxLevelTransition.hallwayName = hallway.name;
            auxLevelTransition.aLevel = "Stage_" + hallway.name.Split('-')[0];
            auxLevelTransition.bLevel = "Stage_" + hallway.name.Split('-')[1];
            auxLevelTransition.AToBTrigger = hallways.transform.Find(hallway.name).GetChild(0).GetComponent<BoxCollider2D>();
            auxLevelTransition.BToATrigger = hallways.transform.Find(hallway.name).GetChild(1).GetComponent<BoxCollider2D>();
            //Calculamos la distancia entre los trigger en forma de vector
            Vector2 distance = auxLevelTransition.BToATrigger.transform.position - auxLevelTransition.AToBTrigger.transform.position;
            distance = distance.normalized;
            
            //Hallamos la forma del trigger, horizontal o vertical
            float triggerWidth = auxLevelTransition.AToBTrigger.size.x;
            float triggerHeight = auxLevelTransition.AToBTrigger.size.y;

            //Si tienen forma horizontal
            if (triggerWidth > triggerHeight) {
                if (distance.y > 0) { //Si A está más arriba que B
                    auxLevelTransition.AFromBEntrance = EntrancePosition.TOP;
                    auxLevelTransition.BFromAEntrance = EntrancePosition.BOTTOM;
                } else { //Si A está más abajo que B
                    auxLevelTransition.AFromBEntrance = EntrancePosition.BOTTOM;
                    auxLevelTransition.BFromAEntrance = EntrancePosition.TOP;
                }
            } else if (triggerHeight > triggerWidth) { //Si tienen forma vertical
                if (distance.x > 0) { //Si A está más a la derecha que B
                    auxLevelTransition.AFromBEntrance = EntrancePosition.RIGHT;
                    auxLevelTransition.BFromAEntrance = EntrancePosition.LEFT;
                } else { //Si A está más a la izquierda que B
                    auxLevelTransition.AFromBEntrance = EntrancePosition.LEFT;
                    auxLevelTransition.BFromAEntrance = EntrancePosition.RIGHT;
                }
            }
            newLevelLoadTriggerManager.levelTransition = auxLevelTransition;
            newLevelLoadTriggerManager.AssignTriggers(); 
            levelTransitions.Add(auxLevelTransition);
        }
        return levelTransitions;
    }
}
