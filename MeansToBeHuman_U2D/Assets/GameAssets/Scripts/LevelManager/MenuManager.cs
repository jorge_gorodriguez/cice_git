﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    Animator animatorL;
    Animator animatorR;

    // Use this for initialization
    void Start () {
        animatorL = transform.Find("PlayerL").GetComponent<Animator>();
        animatorL.SetBool("idle", true);
        animatorR = transform.Find("PlayerR").GetComponent<Animator>();
        animatorL.SetBool("idle", false);
    }
    public void StartGame() {
        SceneManager.LoadScene(1);
    }

    public void ExitGame() {
        Application.Quit();
    }
}
