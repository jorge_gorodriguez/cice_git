﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoadTrigger : MonoBehaviour {

    public string levelToLoad;
    public EntrancePosition entrancePosition;
    Transform exitTransform;

    float horizontalOffsetAdjust = 1;
    float verticalOffsetAdjust = 1;

    private void Start() {
        int exitTransformIndex = this.transform.GetSiblingIndex() == 0 ? 1 : 0;
        exitTransform = this.transform.parent.GetChild(exitTransformIndex);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Player") {
            StartCoroutine(FadeTo(Camera.main.GetComponentInChildren<SpriteRenderer>(), 1, 0.6f, true));
            SetCameraAndPlayer();
        }
    }

    public void SetCameraAndPlayer() {
        PlayerManager.Instance.GetComponent<Rigidbody2D>().simulated  = false;
        PlayerManager.Instance.transform.position = GetSpawnPosition();
        List<GameObject> virtualCameras = new List<GameObject>();
        Transform camerasParent = Camera.main.transform.parent;
        for (int i = 1; i < camerasParent.childCount ; i++) {
            virtualCameras.Add(camerasParent.GetChild(i).gameObject);
        }
        GameObject targetVirtualCamera = null;
        foreach (GameObject virtualCamera in virtualCameras) {
            virtualCamera.SetActive(false);
            if (virtualCamera.name.Contains(levelToLoad.Split('_')[1])) {
                targetVirtualCamera = virtualCamera;
            }
        }
        targetVirtualCamera.SetActive(true);
        PlayerManager.Instance.GetComponent<Rigidbody2D>().simulated = true;
    }

    public Vector2 GetSpawnPosition() {
        Vector2 newPosition = new Vector2(exitTransform.position.x, exitTransform.position.y);
        Vector2 horizontalOffset = new Vector2(horizontalOffsetAdjust, 0f);
        Vector2 verticalOffset = new Vector2(0f, verticalOffsetAdjust);
        switch (entrancePosition) {
            case EntrancePosition.LEFT: {
                newPosition += horizontalOffset;
                break;
            }
            case EntrancePosition.TOP: {
                newPosition -= verticalOffset;
                break;
            }
            case EntrancePosition.RIGHT: {
                newPosition -= horizontalOffset;
                break;
            }
            case EntrancePosition.BOTTOM: {
                newPosition += verticalOffset;
                newPosition += horizontalOffset;
                break;
            }
        }
        return newPosition;
    }

    IEnumerator FadeTo(SpriteRenderer spriteRenderer, float targetOpacity, float duration, bool fadeIn) {
        // Track how many seconds we've been fading.
        float t = 0;
        Color color = spriteRenderer.color;
        float startOpacity = color.a;
        t = 0;
        while (t < duration) {
            t += Time.deltaTime;
            // Turn the time into an interpolation factor between 0 and 1.
            float blend = Mathf.Clamp01(t / duration);
            color.a = Mathf.Lerp(startOpacity, targetOpacity, blend);
            spriteRenderer.color = color;
            yield return null;
        }
        yield return new WaitForSeconds(duration);
        if (fadeIn) {
            StartCoroutine(FadeTo(Camera.main.GetComponentInChildren<SpriteRenderer>(), 0, duration, false));
        }        
    }


}
