﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HangingPlatform : MonoBehaviour {

    public SpriteRenderer mainString;
    public GameObject stringPiecePrefab;
    public int piecesAmount;

    private void Start() {
        //Ignore Collision between 10-Player and 13-String
        Physics2D.IgnoreLayerCollision(10, 13);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Sword") {
            this.GetComponent<SpringJoint2D>().breakForce = 0;
            Destroy(mainString.gameObject);
            for (int i = 0; i < piecesAmount; i++) {
                Quaternion newRandomRotation = Quaternion.Euler(0f, 0f, Random.Range(0,360));
                GameObject stringPiece = Instantiate<GameObject>(stringPiecePrefab, collision.transform.position, newRandomRotation);
                stringPiece.GetComponent<SpriteRenderer>().color = mainString.color;
                Destroy(stringPiece, 1f);                
                Destroy(this);
            }
        }
    }
}
