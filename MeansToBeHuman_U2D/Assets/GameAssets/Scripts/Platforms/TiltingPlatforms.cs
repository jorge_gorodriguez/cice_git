﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TiltingPlatforms : MonoBehaviour {

    List<SpriteRenderer> spriteRenderers;

    public float fadeInterval = 1f;
    float elapsedTime = 0f;
    bool isFading = true;
    
    private void Start() {
        spriteRenderers = new List<SpriteRenderer>(this.GetComponentsInChildren<SpriteRenderer>(true));        
    }
    private void Update() {
        elapsedTime += Time.deltaTime;
        if (elapsedTime >= fadeInterval) {
            elapsedTime = 0;
            isFading = !isFading;
            foreach (SpriteRenderer spriteRenderer in spriteRenderers) {
                if (isFading) {
                    StartCoroutine(FadeTo(spriteRenderer, 0, fadeInterval));
                } else {
                    StartCoroutine(FadeTo(spriteRenderer, 1, fadeInterval));
                }
            }            
        }
    }

    IEnumerator FadeTo(SpriteRenderer spriteRenderer, float targetOpacity, float duration) {
        // Track how many seconds we've been fading.
        float t = 0;
        Color color = spriteRenderer.color;
        float startOpacity = color.a;
        t = 0;
        this.GetComponent<BoxCollider2D>().enabled = isFading;
        while (t < duration) {
            t += Time.deltaTime;
            // Turn the time into an interpolation factor between 0 and 1.
            float blend = Mathf.Clamp01(t / duration);
            color.a = Mathf.Lerp(startOpacity, targetOpacity, blend);
            spriteRenderer.color = color;
            yield return null;
        }
        this.GetComponent<BoxCollider2D>().enabled = !isFading;
    }    
}
