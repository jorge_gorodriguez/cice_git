﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script que controla las plataformas móviles
public class MovingPlatform : MonoBehaviour {

    public GameObject playerHolder;
    Animator animator;

    private void Start() {
        animator = transform.GetComponentInParent<Animator>();
    }

    //Emparentamos al jugador con la plataforma
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.transform.tag == "Player") {
            collision.transform.SetParent(this.transform);
        }
    }

    //Lo liberamos y dejamos emparentado como estaba originalmente
    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.transform.tag == "Player") {
            collision.transform.SetParent(playerHolder.transform);
            //Dejamos la rotación del player a 0, por si la plataforma tiene movimientos
            Quaternion currentRotation = collision.transform.rotation;
            collision.transform.rotation = Quaternion.Euler(currentRotation.x, currentRotation.y, 0f);
        }
    }
}
