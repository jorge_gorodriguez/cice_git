﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Plataforma que se cae tras un tiempo en contacto con ella, haciendo fadeout y reinstanciando otra con las mismas propiedades
public class FallingPlatform : MonoBehaviour {

    public float fadeDuration = 2f;
    public float respawnTime = 5f;
    public float timeToBreak = 2f;
    float timeOnPlatform = 0;
    bool isNew = false;
    bool isFalling = false;

 
    FixedJoint2D fixedJoint2D;
    List<SpriteRenderer> spriteRenderers;
    Transform parent;
    GameObject initialGameObject;

    
    private void Awake() {
        parent = this.transform.parent;
        initialGameObject = this.gameObject;
    }

    void Start () {
        fixedJoint2D = GetComponent<FixedJoint2D>();
        spriteRenderers = new List<SpriteRenderer>(this.GetComponentsInChildren<SpriteRenderer>(true));
        if (isNew) {
            FadeSprites(spriteRenderers, true, fadeDuration);
            this.GetComponent<BoxCollider2D>().enabled = true;
        }
        timeOnPlatform = 0;
    }

    //Contamos el tiempo transcurrido desde que isFalling se activó
    private void Update() {
        if (isFalling) {
            timeOnPlatform += Time.deltaTime;
            if (timeOnPlatform >= timeToBreak) { //Si superamos el tiempo máximo, hacemos rompible la plataforma (para que caiga)
                fixedJoint2D.breakForce = 1f;
            }
        }
    }

    //Cuando contactamos al jugador, activamos el flag isFalling, para que empiece a contar el tiempo
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.transform.tag == "Player") {
            isFalling = true;            
        }
    }

    //Cuando se rompe el joint, recogemos parte de su información, empezamos a fadear y reinstanciamos el objeto
    void OnJointBreak2D(Joint2D brokenJoint) {
        timeOnPlatform = 0;
        FadeSprites(spriteRenderers, false, fadeDuration);
        Vector3 initialPosition = this.transform.position;
        Reinstantiate(initialPosition);        
        Destroy(this.gameObject, respawnTime + 1);
    }

    //Fadeamos cada uno de los sprites de los que se compone la plataforma
    public void FadeSprites(List<SpriteRenderer> spriteRenderers, bool fadeIn, float fadeDuration) {
        float endFadeValue = fadeIn ? 1f : 0f;
        foreach (SpriteRenderer spriteRenderer in spriteRenderers) {
            spriteRenderer.GetComponentInParent<BoxCollider2D>().enabled = false;
            StartCoroutine(FadeTo(spriteRenderer, endFadeValue, fadeDuration));
            if (fadeIn) {
                spriteRenderer.GetComponentInParent<BoxCollider2D>().enabled = true;
            }
        }
    }

    //Reinstanciamos con los datos que hemos ido recogiendo, otra plataforma igual, en la misma posición, y de momento irrompible
    public void Reinstantiate(Vector3 newPosition) {
        StartCoroutine(Respawn(respawnTime, newPosition));
    }    
    IEnumerator Respawn(float waitTime, Vector3 newPosition) {
        yield return new WaitForSeconds(waitTime);
        GameObject aux = Instantiate<GameObject>(initialGameObject, parent);
        aux.transform.position = newPosition;
        FallingPlatform auxFallingPlatform = aux.GetComponent<FallingPlatform>();
        auxFallingPlatform.isNew = true;
        auxFallingPlatform.fadeDuration = this.fadeDuration;
        auxFallingPlatform.respawnTime = this.respawnTime;
        auxFallingPlatform.timeToBreak = this.timeToBreak;
        if (aux.GetComponent<FixedJoint2D>()) {
            aux.GetComponent<FixedJoint2D>().breakForce = 999999f;
        } else {
            aux.AddComponent<FixedJoint2D>();
            aux.GetComponent<FixedJoint2D>().breakForce = 999999f;
        }
        List<SpriteRenderer> auxSpriteRenderers = new List<SpriteRenderer>(aux.GetComponentsInChildren<SpriteRenderer>(true));
        FadeSprites(auxSpriteRenderers, false, 0);
    }

    IEnumerator FadeTo(SpriteRenderer spriteRenderer, float targetOpacity, float duration) {
        float elapsedTime = 0;
        Color color = spriteRenderer.color;
        float startOpacity = color.a;
        while (elapsedTime < duration) {
            elapsedTime += Time.deltaTime;
            float blend = Mathf.Clamp01(elapsedTime / duration);
            color.a = Mathf.Lerp(startOpacity, targetOpacity, blend);
            spriteRenderer.color = color;
            yield return null;
        }
    }

    private void OnDestroy() {
        isFalling = false;
    }
}
