﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickableType {
    HEALTH, COIN
}

public class PickableItem : MonoBehaviour {
    public PickableType pickableType;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.tag == "Player") {
            Debug.Log("Player coin");
            switch (pickableType) {
                case PickableType.HEALTH:
                    PlayerManager.Instance.HealDamage(100);
                    break;
                case PickableType.COIN:
                    PlayerManager.Instance.ChangeCoins(25);
                    break;
            }
        }
        Destroy(this.gameObject);
    }

}
