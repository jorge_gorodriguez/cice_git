﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EjemploDobleSalto : MonoBehaviour {

    public bool onGround = false;

    public bool dobleJump = true;

    public float jumpForce = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space) ) {
            if (onGround) {
               
                Saltar();
            } else {
                if (dobleJump == true) {
                    Saltar();
                    dobleJump = false;
                }
            }
            
            
        }

	}

    public void Saltar() {
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        onGround = true;
        dobleJump = true;
    }
    private void OnCollisionExit2D(Collision2D collision) {
        onGround = false;
    }
}
